{
  inputs = {
    nixpkgs.url = "git+file:///home/scvalex/repo/infra/nix-channel/nixpkgs";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nixos-dns = {
      url = "github:Janik-Haag/nixos-dns";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    kb-classic = {
      url = "gitlab:abstract-binary/kubebro/flake";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.utils.follows = "flake-utils";
    };
    ldgr = {
      url = "gitlab:scvalex/ldgr";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    env-sensor = {
      url = "git+https://codeberg.org/scvalex/env-sensor.git";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixpkgs-stable.follows = "nixpkgs";
    };
    colmena = {
      url = "github:zhaofengli/colmena";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    nixos-anywhere = {
      url = "github:nix-community/nixos-anywhere";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.nixos-stable.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    picotron = {
      url = "gitlab:scvalex/picotron-flake";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    yazi = {
      # url = "github:sxyazi/yazi/v25.3.2";
      url = "github:sxyazi/yazi";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };
  outputs =
    {
      self,
      nixpkgs,
      nixpkgs-unstable,
      flake-utils,
      kb-classic,
      ldgr,
      nixos-hardware,
      emacs-overlay,
      env-sensor,
      colmena,
      nixos-anywhere,
      disko,
      picotron,
      nixos-dns,
      yazi,
      ...
    }:
    (
      {
        colmena = import ./deployment.nix {
          inherit
            kb-classic
            ldgr
            nixos-hardware
            emacs-overlay
            env-sensor
            picotron
            yazi
            ;
          nixpkgs0 = nixpkgs;
          nixpkgs-unstable0 = nixpkgs-unstable;
        };
        colmenaHive = colmena.lib.makeHive self.outputs.colmena;
        nixosConfigurations = {
          # The following configurations are only used during the
          # initial deployment.  Afterwards, the system configuration is
          # taken from the `colmena` section above.
          #
          # Deploy: nixos-anywhere --disk-encryption-keys /tmp/zpool-encryption.key ./secrets/passwords/zpool-encryption.key --flake .#bootstrap-hetzner-cloud-server root@NNN.NNN.NNN.NNN

          bootstrap-hetzner-cloud-server = import ./bootstrap-hetzner-cloud-server.nix {
            inherit nixpkgs disko;
            system = "aarch64-linux";
            hostName = "nur-aws-vpn5";
            hostId = "88789f95";
            publicInterface = "enp1s0";
          };
        };
      }
      // flake-utils.lib.eachDefaultSystem (
        system:
        let
          pkgs = import nixpkgs {
            inherit system;
            config.allowUnfree = true;
          };
          disabledFrontendsList = import ./disabled-frontends.nix;
          machines = import ./machines.nix { inherit pkgs; };
          generateDns = nixos-dns.utils.generate pkgs;
          dnsConfig = {
            extraConfig = import ./dns.nix { inherit pkgs machines disabledFrontendsList; };
          };
        in
        {
          apps.genCerts = {
            type = "app";
            program = import ./gen-certs.nix pkgs machines;
          };
          apps.showCert = {
            type = "app";
            program = toString (
              pkgs.writers.writeBash "show-cert" ''
                if [[ $# != 1 ]]; then
                   echo "ERROR: Specify certificate argument"
                   exit 1
                fi
                CERT="$1"
                ${pkgs.openssl}/bin/openssl x509 -text -noout -in "$CERT"
              ''
            );
          };

          # nix eval .#dnsDebugConfig
          dnsDebugConfig = nixos-dns.utils.debug.config dnsConfig;
          packages.octodns-config = generateDns.octodnsConfig {
            inherit dnsConfig;
            config = {
              providers = {
                hetzner = {
                  class = "octodns_hetzner.HetznerProvider";
                  token = "env/HETZNER_DNS_API";
                };
              };
            };
            zones = {
              "forthinvesting.com." = nixos-dns.utils.octodns.generateZoneAttrs [ "hetzner" ];
              "marketunpack.com." = nixos-dns.utils.octodns.generateZoneAttrs [ "hetzner" ];
              "scvalex.net." = nixos-dns.utils.octodns.generateZoneAttrs [ "hetzner" ];
              "abstractbinary.org." = nixos-dns.utils.octodns.generateZoneAttrs [ "hetzner" ];
              "bugetuldestat.ro." = nixos-dns.utils.octodns.generateZoneAttrs [ "hetzner" ];
            };
          };
          apps.octodns = {
            type = "app";
            program = toString (
              pkgs.writers.writeBash "apply" ''
                octodns-sync --quiet --config-file=${
                  self.outputs.packages."${system}".octodns-config
                } --doit --force
              ''
            );
          };

          devShell =
            with pkgs;
            mkShell {
              buildInputs = [
                cfssl
                colmena.defaultPackage."${system}"
                difftastic
                just
                kubernetes-helm
                nixos-anywhere.packages."${system}".default
                openssl
                octodns
                octodns-providers.bind
                octodns-providers.hetzner
              ];
              GIT_EXTERNAL_DIFF = "${difftastic}/bin/difft";
            };
        }
      )
    );
}
