{
  config,
  modulesPath,
  lib,
  pkgs,
  ...
}:

with lib;
let
  cfg = config.ab.machine;
in
{
  imports = [
    (modulesPath + "/profiles/qemu-guest.nix")
    ./ab-machine.nix
    ./ab-server.nix
    ./hetzner-cloud-static-networking.nix
  ];

  options = { };

  config = {
    assertions = [
      {
        assertion = config.networking.hostId != null;
        message = "Must set config.networking.hostId for ZFS to work";
      }
    ];

    fileSystems = {
      "/" = {
        device = "zroot/nixos";
        fsType = "zfs";
      };
      "/boot" = {
        device = "/dev/disk/by-partlabel/BOOT";
        fsType = "vfat";
        options = [ "nofail" ];
      };
    };

    ab.machine.encryptedRoot = mkDefault true;
    ab.machine.zpools = mkDefault [ "zroot" ];

    boot.loader =
      if cfg.preferSystemdBoot then
        {
          systemd-boot.enable = true;
          efi.canTouchEfiVariables = true;
        }
      else
        {
          systemd-boot.enable = false;
          grub = {
            enable = true;
            efiSupport = false;
            copyKernels = true;

            devices = [ cfg.singleBootDevice ];
          };
        };

    boot.initrd.availableKernelModules = [
      "ata_piix"
      "sd_mod"
      "sr_mod"
      "virtio"
      "virtio_net"
      "virtio_pci"
      "virtio_scsi"
    ];
    boot.initrd.kernelModules = [ "virtio_gpu" ];
    boot.supportedFilesystems = [ "zfs" ];

    services.zfs.autoScrub.enable = true;
    services.zfs.trim.enable = true;

    boot.kernelParams = [
      "console=tty"
      # See <https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt> for docs on this
      # ip=<client-ip>:<server-ip>:<gw-ip>:<netmask>:<hostname>:<device>:<autoconf>:<dns0-ip>:<dns1-ip>:<ntp0-ip>
      "ip=::::${config.networking.hostName}-initrd:${config.ab.networking.publicInterface}:dhcp:"
    ];
    boot.initrd.network = {
      enable = true;
      ssh = {
        enable = true;
        port = 2222;
        # hostKeys paths must be unquoted strings, otherwise you'll run into issues
        # with boot.initrd.secrets the keys are copied to initrd from the path specified;
        # multiple keys can be set you can generate any number of host keys using
        # 'ssh-keygen -t ed25519 -N "" -f /boot-1/initrd-ssh-key'
        hostKeys = [ "/boot/initrd-ssh-key" ];
        # public ssh key used for login
        authorizedKeys = [ (readFile ./secrets/ssh/infra.pub) ];
      };
      # this will automatically load the zfs password prompt on login
      # and kill the other prompt so boot can continue
      postCommands = ''
        cat <<EOG > /root/.profile
        while ! pgrep -x "zfs" > /dev/null; do
          echo "Zfs not running yet. Waiting..."
          sleep 1
        done
        ${concatMapStrings (zpool: "zpool import -f ${zpool} || true\n") cfg.zpools}
        zfs load-key -a
        killall zfs
        EOG
      '';
    };
  };
}
