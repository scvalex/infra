{
  config,
  lib,
  pkgs,
  nodes,
  ...
}:

with lib;
let
  cfg = config.ab.services.wireguard;
  netCfg = config.ab.networking;
  machineCfg = config.ab.machine;
  wgPrivKeyEtcPath = "wireguard/key.priv";
  wireguardKeyFile =
    if machineCfg.encryptedRoot then "/etc/${wgPrivKeyEtcPath}" else "/run/keys/wireguard";
  ips = [
    "${cfg.ipAddress}/24"
    "${cfg.ip6Address}/64"
  ];
  wgFwMark = 4242;
  wgTable = 4000;
in
{
  imports = [
    ./ab-machine.nix
    ./ab-networking.nix
  ];

  options = {
    ab.services.wireguard = {
      enabled = mkOption {
        type = types.bool;
        default = true;
        description = "Whether to configure Wireguard on this machine";
      };

      wgInterface = mkOption {
        type = types.str;
        default = "wg0";
        description = "Wireguard interface name";
      };

      ipAddress = mkOption {
        type = with types; nullOr str;
        description = "Wireguard IPv4 address";
        default = null;
      };

      ip6Address = mkOption {
        type = types.str;
        description = "Wireguard IPv6 address";
      };

      publicKey = mkOption {
        type = with types; nullOr str;
        description = "Wireguard public key";
        default = null;
      };

      privateKey = mkOption {
        type = types.str;
        description = "Wireguard private key";
      };

      routeEverythingThroughWg = mkOption {
        type = types.bool;
        description = "Whether to route all outgoing traffic through Wireguard";
        default = false;
      };

      routeThroughPeer = mkOption {
        type = with types; attrsOf (listOf str);
        default = { };
        example = {
          "nur-qws-vpn3" = [
            "0.0.0.0/0"
            "::/0"
          ];
        };
        description = "Route the given subnets through this Wireguard peer";
      };
    };
  };

  config = mkIf cfg.enabled {
    environment.systemPackages = with pkgs; [ wireguard-tools ];

    boot.kernelModules = [ "wireguard" ];
    networking.wireguard.enable = true;
    networking.firewall.trustedInterfaces = [ cfg.wgInterface ];
    networking.firewall.allowedUDPPorts = [
      # wireguard on low port number to avoid surprises like
      # https://status.hetzner.com/incident/129728ce-ba25-49b6-96cc-aafcd39ab0b7
      8123
      51820 # wireguard
    ];

    deployment.keys.wireguard = mkIf (!machineCfg.encryptedRoot) {
      text = cfg.privateKey;
      user = "systemd-network";
    };

    environment.etc = mkIf machineCfg.encryptedRoot {
      "${wgPrivKeyEtcPath}" = {
        text = cfg.privateKey;
        mode = "0400";
        user = "systemd-network";
      };
    };

    # TODO Get rid of this paths thing once all machines have
    # encrypted roots.
    systemd.paths.wireguard-key-watcher = mkIf (!machineCfg.encryptedRoot) {
      description = "WireGuard Tunnel - ${cfg.wgInterface} - Private Key";
      wantedBy = [ "multi-user.target" ];
      pathConfig = {
        PathModified = wireguardKeyFile;
      };
    };

    systemd.services.wireguard-key-watcher = mkIf (!machineCfg.encryptedRoot) {
      wantedBy = [ "multi-user.target" ];
      description = "WireGuard Tunnel - ${cfg.wgInterface} - Private Key";
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${pkgs.systemd}/bin/systemctl restart systemd-networkd";
      };
    };

    systemd.network = {
      netdevs."10-${cfg.wgInterface}" = {
        netdevConfig = {
          Kind = "wireguard";
          Name = cfg.wgInterface;
          MTUBytes = "1420";
        };
        wireguardConfig = {
          PrivateKeyFile = wireguardKeyFile;
          ListenPort = 8123;
          FirewallMark = wgFwMark;
          RouteTable = "off";
        };
        wireguardPeers =
          mapAttrsToList
            (
              peerHostname: peer:
              let
                endpoint =
                  let
                    peerNetCfg = peer.config.ab.networking;
                  in
                  if peerNetCfg.ipAddress != null then
                    "${peerNetCfg.ipAddress}:8123"
                  else if peerNetCfg.lan == netCfg.lan && peerNetCfg.lanIpAddress != null then
                    "${peerNetCfg.lanIpAddress}:8123"
                  else
                    null;
              in
              {
                AllowedIPs =
                  let
                    peerIps = [
                      (peer.config.ab.services.wireguard.ipAddress + "/32")
                      (peer.config.ab.services.wireguard.ip6Address + "/128")
                    ];
                  in
                  if cfg.routeThroughPeer ? "${peerHostname}" then
                    (cfg.routeThroughPeer.${peerHostname} ++ peerIps)
                  else
                    peerIps;
                PublicKey = peer.config.ab.services.wireguard.publicKey;
                RouteTable = "off";
              }
              // (if endpoint != null then { Endpoint = endpoint; } else { })
            )
            (
              filterAttrs (
                peerHostname: peer:
                peerHostname != config.networking.hostName && peer.config.ab.services.wireguard.publicKey != null
              ) nodes
            );
      };

      networks."50-${cfg.wgInterface}" = {
        matchConfig.Name = cfg.wgInterface;
        address = ips;
        DHCP = "no";
        networkConfig = {
          IPv6AcceptRA = false;
        };
        routingPolicyRules = mkIf cfg.routeEverythingThroughWg [
          {
            Family = "both";
            Table = "main";
            SuppressPrefixLength = 0;
            Priority = 10;
          }
          {
            Family = "both";
            InvertRule = true;
            FirewallMark = wgFwMark;
            Table = wgTable;
            Priority = 11;
          }
        ];
        routes = mkIf cfg.routeEverythingThroughWg [
          {
            Destination = "0.0.0.0/0";
            Table = wgTable;
            Scope = "link";
          }
          {
            Destination = "::/0";
            Table = wgTable;
            Scope = "link";
          }
        ];
        linkConfig.RequiredForOnline = false;
      };
    };

    networking.nftables.ruleset = mkIf cfg.routeEverythingThroughWg ''
      table inet wg-wg0 {
        chain preraw {
          type filter hook prerouting priority raw; policy accept;
          iifname != "${cfg.wgInterface}" ip daddr ${cfg.ipAddress} fib saddr type != local drop
          iifname != "${cfg.wgInterface}" ip6 daddr ${cfg.ip6Address} fib saddr type != local drop
        }
        chain premangle {
          type filter hook prerouting priority mangle; policy accept;
          meta l4proto udp meta mark set ct mark
        }
        chain postmangle {
          type filter hook postrouting priority mangle; policy accept;
          meta l4proto udp meta mark ${toString wgFwMark} ct mark set meta mark
        }
      }
    '';
  };
}
