{ config, lib, ... }:
with lib;
let
  cfg = config.ab.services.zfs-backup-sink;
in
{
  imports = [ ./ab-machine.nix ];

  options = {
    ab.services.zfs-backup-sink = {
      enable = mkEnableOption "ZFS backup sink";

      sshKey = mkOption {
        description = mdDoc ''
          Private SSH key to use to login to syncoid user on target machines
        '';
        type = types.path;
      };

      pairs = mkOption {
        description = mdDoc "Pairs of source and destinations to sync.";
        type =
          with types;
          attrsOf (submodule {
            options = {
              source = mkOption {
                description = mdDoc "The source dataset.  Can be local or remote.";
                type = str;
                example = "syncoid@nur-apr-app1:zroot/mysql";
              };
              target = mkOption {
                description = mdDoc "The target dataset.  Can be local or remote.";
                type = str;
                example = "zdata/backup/mysql";
              };
            };
          });
      };
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = config.ab.machine.encryptedRoot;
        message = "ZFS sink machines must have encrypted roots";
      }
    ];

    services.syncoid = {
      enable = true;
      commonArgs = [
        "--no-sync-snap"
        "--sshoption"
        "StrictHostKeyChecking=no"
        "--delete-target-snapshots"
      ];
      localTargetAllow = [
        "create"
        "mount"
        "receive"
        "destroy"
      ];
      localSourceAllow = [ "send" ];
      sshKey = "/etc/syncoid/syncoid-ssh-key";
      commands = cfg.pairs;
    };

    environment.etc."syncoid/syncoid-ssh-key" = {
      text = readFile ./secrets/ssh/syncoid;
      user = "syncoid";
      group = "syncoid";
    };
  };
}
