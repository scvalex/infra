{
  nixpkgs0,
  nixpkgs-unstable0,
  kb-classic,
  ldgr,
  nixos-hardware,
  emacs-overlay,
  env-sensor,
  picotron,
  yazi,
  ...
}:
{
  meta =
    let
      nixpkgs = import nixpkgs0 {
        system = "x86_64-linux";
        overlays = [ ];
      };
      nixpkgs-unstable = import nixpkgs-unstable0 {
        system = "x86_64-linux";
        overlays = [ ];
      };
    in
    {
      inherit nixpkgs;
      description = "boxes to run arbitrary things on";
      specialArgs = {
        machines = import ./machines.nix { pkgs = nixpkgs; };
        inherit
          nixpkgs0
          nixpkgs-unstable0
          nixpkgs-unstable
          env-sensor
          nixos-hardware
          ldgr
          kb-classic
          picotron
          yazi
          ;
      };
      nodeNixpkgs = {
        # cibo = nixpkgs-unstable;
      };
      allowApplyAll = false;
    };

  defaults =
    {
      pkgs,
      config,
      nixpkgs0,
      ...
    }:
    {
      imports = [
        ./ab-build-machine.nix
        ./ab-email.nix
        ./ab-etcd.nix
        ./ab-grafana-agent.nix # Not used at the moment
        ./ab-healthchecks.nix
        ./ab-iot-hub.nix
        ./ab-kubernetes.nix
        ./ab-machine.nix
        ./ab-networking.nix
        ./ab-pihole.nix
        ./ab-prometheus.nix
        ./ab-smartd.nix
        ./ab-ssl.nix
        ./ab-wireguard.nix
        ./ab-zfs-backup-sink.nix
        ./ipvs.nix
      ];

      nix = {
        # package = pkgs.nixUnstable; # or versioned attributes like nix_2_4
        extraOptions = ''
          experimental-features = nix-command flakes
        '';
        registry.nixpkgs-unstable.flake = nixpkgs-unstable0;
      };

      nixpkgs.overlays = [
        (self: super: { grafana = super.grafana.overrideAttrs { doCheck = false; }; })
        emacs-overlay.overlay
      ];

      # TODO OpenSSH RCE: Remove this once
      # https://github.com/NixOS/nixpkgs/pull/323753 gets merged.
      services.openssh.settings.LoginGraceTime = 0;

      # BEGIN COMMON LIST OF PACKAGES
      environment.systemPackages = with pkgs; [
        atop
        binutils
        dogdns
        eza
        fd
        file
        fzf
        git
        gnupg
        htop
        iftop
        iotop
        jq
        libossp_uuid
        linuxHeaders
        # linuxPackages.bcc
        # linuxPackages.bpftrace
        lsof
        mtr
        ncdu
        netcat
        nftables
        nss
        parallel
        pciutils
        perf-tools
        psmisc
        pv
        rclone
        ripgrep
        strace
        tcpdump
        tmux
        tree
        unzip
        usbutils
        vim
        wget
        zoxide
      ];
      # END COMMON LIST OF PACKAGES
    };

  cibo = import ./hosts/cibo.nix;
  # edb-qws-pi2 = import ./hosts/edb-qws-pi2.nix;
  fp4 = import ./hosts/unmanaged.nix;
  fsn-qpr-kube1 = import ./hosts/fsn-qpr-kube1.nix;
  fsn-qpr-kube2 = import ./hosts/fsn-qpr-kube2.nix;
  fsn-qpr-kube3 = import ./hosts/fsn-qpr-kube3.nix;
  laptop = import ./hosts/laptop.nix;
  nur-apr-app1 = import ./hosts/unmanaged.nix;
  nur-apr-bgt1 = import ./hosts/unmanaged.nix;
  nur-aws-vpn5 = import ./hosts/nur-aws-vpn5.nix;
  nur-apr-matrix1 = import ./hosts/nur-apr-matrix1.nix;
  phone2 = import ./hosts/unmanaged.nix;
  sana = import ./hosts/sana.nix;
}
