{
  pkgs,
  lib,
  config,
  ...
}:
with lib;
let
  cfg = config.ab.services.build-machine;
in
{
  options = {
    ab.services.build-machine = {
      enable = mkEnableOption "distributed build machine";
    };
  };

  config = mkIf cfg.enable {
    users.users.nixdist = {
      isSystemUser = true;
      createHome = true;
      home = "/home/nixdist";
      openssh.authorizedKeys.keyFiles = [ ./secrets/ssh/nixdist.pub ];
      group = "nixdist";
      useDefaultShell = true;
    };
    users.groups.nixdist = { };
    nix.settings.trusted-users = [ "nixdist" ];
  };
}
