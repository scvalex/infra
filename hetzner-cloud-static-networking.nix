{ lib, config, ... }:
with lib;
let
  netCfg = config.ab.networking;
in
{
  imports = [ ./ab-networking.nix ];

  config = {
    networking.nameservers = [
      "185.12.64.1"
      "185.12.64.2"
    ];

    systemd.network.networks."10-wan" = {
      matchConfig.Name = netCfg.publicInterface;
      linkConfig.RequiredForOnline = "routable";
      DHCP = "no";
      address = [
        netCfg.ipAddress
        netCfg.ip6Address
      ];
      routes = [
        {
          Gateway = "fe80::1";
        }
        {
          Gateway = "172.31.1.1";
          GatewayOnLink = true;
        }
      ];
    };
  };
}
