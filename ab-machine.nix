{
  config,
  lib,
  pkgs,
  nixpkgs-unstable0,
  nodes,
  ...
}:

with lib;
let
  cfg = config.ab.machine;
  pkgs-unstable = import nixpkgs-unstable0 {
    system = config.nixpkgs.system;
    overlays = [ ];
  };
in
{
  imports = [ ./ab-healthchecks.nix ];

  options = {
    ab.machine = {
      isWorkstation = mkOption {
        type = types.bool;
        default = false;
        description = "Whether this machine is a workstation (with dynamic users)";
      };

      swap = mkOption {
        type = with types; nullOr path;
        default = null;
        description = "Path to swap partition (if any)";
      };

      encryptedRoot = mkOption {
        type = types.bool;
        default = false;
        description = "Whether the disks are encrypted.";
      };

      populateHosts = mkOption {
        type = types.bool;
        default = true;
        description = "Whether to populate the hosts file with the hostnames of all machines";
      };

      singleBootDevice = mkOption {
        type = types.str;
        description = "Use a single boot device.";
      };

      zpools = mkOption {
        type = with types; nullOr (listOf str);
        default = null;
        description = "List of zpools to import";
      };

      preferSystemdBoot = mkOption {
        type = types.bool;
        default = !(config.nixpkgs.system == "x86_64-linux");
        description = ''
          Prefer systemd-boot over the usual.  In practice, the only machines that require
          grub are Hetzner's x86-64 cloud servers.  Their arm64 servers work fine with UEFI.
        '';
      };
    };
  };

  config = {
    assertions = [
      {
        assertion = cfg.encryptedRoot -> cfg.zpools != null;
        message = "Must specify `ab.machine.zpools` if `ab.machine.encryptedRoot` is used.";
      }
      {
        assertion = cfg.preferSystemdBoot -> (cfg.singleBootDevice != null);
        message = "If prefering systemd-boot, we must have a single boot device.";
      }
    ];

    swapDevices = mkIf (cfg.swap != null) [ { device = cfg.swap; } ];

    environment.systemPackages = [ pkgs-unstable.helix ];

    programs.fish.enable = true;
    programs.fish.promptInit = readFile ./fish_prompt.fish;
    programs.fish.interactiveShellInit = ''
      set fish_greeting
      set -gx EDITOR hx
      set -gx ETCDCTL_CERT /etc/kubernetes/secret/etcd_client_pem
      set -gx ETCDCTL_KEY /etc/kubernetes/secret/etcd_client_key
      set -gx ETCDCTL_CACERT /etc/abstractbinary-ca.pem
      set -gx KUBECONFIG /etc/kubernetes/admin.conf
    '';
    users.defaultUserShell = pkgs.fish;

    users.mutableUsers = cfg.isWorkstation;

    zramSwap = {
      enable = true;
      memoryPercent = 25;
    };

    services.journald.extraConfig = ''
      SystemMaxUse=1000M
      SystemMaxFileSize=50M
    '';

    # Cleanup old nix store paths
    nix.gc.automatic = true;
    nix.gc.options = "--delete-old --delete-older-than 30d";

    networking.useNetworkd = true;
    systemd.network.enable = true;
    users.users."systemd-network".extraGroups = [ "keys" ];
    services.resolved.enable = true;

    networking.hosts = optionalAttrs cfg.populateHosts (
      (mapAttrs' (
        hostname: node: nameValuePair node.config.ab.services.wireguard.ipAddress [ "${hostname}.wg" ]
      ) (filterAttrs (_: node: node.config.ab.services.wireguard.ipAddress != null) nodes))
      // (mapAttrs' (
        hostname: node: nameValuePair node.config.ab.networking.ipAddress [ "${hostname}.external" ]
      ) (filterAttrs (_: node: node.config.ab.networking.ipAddress != null) nodes))
      // (mapAttrs' (
        hostname: node: nameValuePair node.config.ab.networking.ip6Address [ "${hostname}.external" ]
      ) (filterAttrs (_: node: node.config.ab.networking.ip6Address != null) nodes))
      // (mapAttrs' (
        hostname: node: nameValuePair node.config.ab.networking.lanIpAddress [ "${hostname}.lan" ]
      ) (filterAttrs (_: node: node.config.ab.networking.lanIpAddress != null) nodes))
    );

    networking.firewall.enable = true;

    networking.nftables.enable = true;

    systemd.services.nix-channel-update = {
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${pkgs.nix}/bin/nix-channel --update";
      };
      startAt = "02:25";
    };

    documentation.nixos.enable = false;
    documentation.man.man-db.enable = config.nixpkgs.system == "x86_64-linux";
  };
}
