{
  pkgs,
  machines,
  disabledFrontendsList,
  ...
}:
with pkgs.lib;
let
  ns = {
    data = [
      "helium.ns.hetzner.de"
      "hydrogen.ns.hetzner.com"
      "oxygen.ns.hetzner.com"
    ];
  };
  mkFrontendRecords =
    domain: field:
    pipe machines [
      (mapAttrs (hostname: machine: mergeAttrs machine { inherit hostname; }))
      attrValues
      (filter (
        machine:
        !(elem machine.hostname disabledFrontendsList) && machine ? domains && elem domain machine.domains
      ))
      (filter (machine: machine ? "${field}"))
      (map (machine: machine."${field}"))
    ];
  mailgunMx = [
    {
      preference = 10;
      exchange = "mxa.eu.mailgun.org";
    }
    {
      preference = 10;
      exchange = "mxb.eu.mailgun.org";
    }
  ];
  fastmailMx = [
    {
      preference = 10;
      exchange = "in1-smtp.messagingengine.com";
    }
    {
      preference = 20;
      exchange = "in2-smtp.messagingengine.com";
    }
  ];
in
{
  defaultTTL = 3600;
  zones = {
    "forthinvesting.com" = {
      "" = {
        inherit ns;
        a.data = [ "157.90.166.239" ];
        aaaa.data = [ "2a01:4f8:c0c:eea4::1" ];
        txt.data = [ "v=spf1 -all" ];
      };
      "*._domainkey".txt.data = "v=DKIM1; p=";
      "_dmarc".txt.data =
        "v=DMARC1;p=reject;sp=reject;adkim=s;aspf=s;fo=1;rua=mailto:dmarc@abstractbinary.org";
    };
    "marketunpack.com" = {
      "" = {
        inherit ns;
        a.data = [ "157.90.166.239" ];
        aaaa.data = [ "2a01:4f8:c0c:eea4::1" ];
        mx.data = fastmailMx;
        txt.data = [
          "v=spf1 include:spf.messagingengine.com ?all"
          "google-site-verification=wuzasK71b-A8rhvG33k13YFeV7sHDXP0B6h41T_yb6g"
        ];
      };
      "mail".mx.data = mailgunMx;
      "mail".txt.data = "v=spf1 include:mailgun.org ~all";
      "29544265e0e1909b6658c1da9b8f8cc2".cname.data = "verify.bing.com";
      "email.mail".cname.data = "eu.mailgun.org";
      "mesmtp._domainkey".cname.data = "mesmtp.marketunpack.com.dkim.fmhosted.com";
      "fm1._domainkey".cname.data = "fm1.marketunpack.com.dkim.fmhosted.com";
      "fm2._domainkey".cname.data = "fm2.marketunpack.com.dkim.fmhosted.com";
      "fm3._domainkey".cname.data = "fm3.marketunpack.com.dkim.fmhosted.com";
      "email._domainkey.mail".txt.data =
        "k=rsa; p=MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0vR7KKGD4Sss3yQV+S1EftOP7g0egC7QJTkasD8xoHM9QX4TyPplIbaqhLTPdCGejjv5aqPzHBFVHBdLZPvdKW2Wrw2AQ+oInVeZmdyjVDftxem6QS1YfneMHQNz24mbklaG5KDG8/ricdA+/hr+pn2SA24kKD5SqXSy3IZ5XEiZRWIXUMC+JQ1EvXPudoYgqRdD/2qVOlFEY9dkaxEJ+CivI5DH7BV7aDTq9rnYcedsyaxo7wSitRAiXOIFMMUoz3f0QN4lCp/2nrborLzQ/J1G9KT9142uCwvWYO4eBWO47fm4yvbOYHxnKoHiEnFpaQku4JCXQNVshqJYJIc15QIDAQAB";
    };
    "scvalex.net" = {
      "" = {
        inherit ns;
        a.data = mkFrontendRecords "scvalex.net" "ipAddress";
        aaaa.data = mkFrontendRecords "scvalex.net" "ip6Address";
        mx.data = fastmailMx;
        txt.data = [
          "v=spf1 include:spf.messagingengine.com ?all"
          "google-site-verification=HotlbsGbNz4kVCUsjBwT0hC3CfS_8Tew7KQ-fnvVCxY"
        ];
      };
      "umami" = {
        a.data = mkFrontendRecords "umami.scvalex.net" "ipAddress";
        aaaa.data = mkFrontendRecords "umami.scvalex.net" "ip6Address";
      };
      "_dmarc".txt.data = "v=DMARC1; p=none;";
      "mesmtp._domainkey".cname.data = "mesmtp.scvalex.net.dkim.fmhosted.com";
      "fm1._domainkey".cname.data = "fm1.scvalex.net.dkim.fmhosted.com";
      "fm2._domainkey".cname.data = "fm2.scvalex.net.dkim.fmhosted.com";
      "fm3._domainkey".cname.data = "fm3.scvalex.net.dkim.fmhosted.com";
      "feedback".txt.data = "v=spf1 include:mailgun.org ~all";
      "feedback".mx.data = mailgunMx;
      "mta._domainkey.feedback".txt.data =
        "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDO5AW3J+NNMGlh6ukt6Ez2D/Dqt4Tn3IzGiOn1Wo+lvV89u0FFXuev5gXsk+EYrw2Kyq9gGwUI0WDwg+XgV8q1npBtErgrZ/NKdUd8kXLaBEqZr3seF76rrlSBUTh6qiccfR5bH48bgDWxQCTxKgmmSSZ94rIZ0Wfg/VcQ3c3U7QIDAQAB";
    };
    "abstractbinary.org" = {
      "" = {
        inherit ns;
        a.data = mkFrontendRecords "abstractbinary.org" "ipAddress";
        aaaa.data = mkFrontendRecords "abstractbinary.org" "ip6Address";
        mx.data = fastmailMx;
        txt.data = [
          "v=spf1 include:spf.messagingengine.com ?all"
        ];
      };
      "foundry-ftp" = {
        a.data = mkFrontendRecords "foundry-ftp.abstractbinary.org" "ipAddress";
        aaaa.data = mkFrontendRecords "foundry-ftp.abstractbinary.org" "ip6Address";
      };
      "foundry" = {
        a.data = mkFrontendRecords "foundry.abstractbinary.org" "ipAddress";
        aaaa.data = mkFrontendRecords "foundry.abstractbinary.org" "ip6Address";
      };
      "files" = {
        a.data = mkFrontendRecords "files.abstractbinary.org" "ipAddress";
        aaaa.data = mkFrontendRecords "files.abstractbinary.org" "ip6Address";
      };
      "matrix" = {
        a.data = [ machines.nur-apr-matrix1.ipAddress ];
        aaaa.data = [ machines.nur-apr-matrix1.ip6Address ];
      };
      "email.mailgun".cname.data = "eu.mailgun.org";
      "mailgun".mx.data = mailgunMx;
      "mailgun".txt.data = "v=spf1 include:mailgun.org ~all";
      "email._domainkey.mailgun".txt.data =
        "k=rsa; p=MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDxQJ5NBDiBgKxm51LzxL5npGy4M143Qibp1PVZgiC9Rpyltkm60xviaLpeL9BBipzghFW6QiqPg5QihfeI3LKz0EbqIM5PT4Cv1ofCVHZ8IsTjP8KWRNtXJ34eVQ4xOW+1/dpAEfMPScvnmcYYy2jw/XyZnShBqWPOxltFTPsqxQIDAQAB";
      "_dmarc".txt.data = "v=DMARC1; p=none;";
      "mesmtp._domainkey".cname.data = "mesmtp.abstractbinary.org.dkim.fmhosted.com";
      "fm1._domainkey".cname.data = "fm1.abstractbinary.org.dkim.fmhosted.com";
      "fm2._domainkey".cname.data = "fm2.abstractbinary.org.dkim.fmhosted.com";
      "fm3._domainkey".cname.data = "fm3.abstractbinary.org.dkim.fmhosted.com";
    };
    "bugetuldestat.ro" = {
      "" = {
        inherit ns;
        a.data = [ machines.nur-apr-bgt1.ipAddress ];
        aaaa.data = [ machines.nur-apr-bgt1.ip6Address ];
        txt.data = [ "v=spf1 -all" ];
      };
      "_dmarc".txt.data =
        "v=DMARC1;p=reject;sp=reject;adkim=s;aspf=s;fo=1;rua=mailto:dmarc-rua@dmarc.service.gov.uk,mailto:dmarc@abstractbinary.org";
      "*._domainkey".txt.data = "v=DKIM1; p=";
    };
  };
}
