# infra

This repo houses the configuration for my infrastructure.  It's not
really meant to be used by anyone else—it's certainly not intended to
fit any general use case.  It's open-source mostly so that I can show
off bits of it.


## Kubernetes management

### Drain a node

```
kubectl drain --ignore-daemonsets --delete-emptydir-data hel-qws-app1
```

### Delete a node

```
# First, remove kubernetes from the host with nixops
# kubectl delete node hel-qws-app1
# kubectl delete nodes.longhorn.io hel-qws-app1 -n longhorn-system
```

## General apply

```
kubectl apply -k kube/
```

## General regen/update

```
make -C kube/ update
```

## Labels

### Longhorn

```
kubectl label nodes fsn-qws-app3 node.longhorn.io/create-default-disk=true
```

### zfs-localpv

```
kubectl label nodes fsn-qws-app2 zfs-localpv=enabled --overwrite
kubectl label nodes fsn-qws-app3 zfs-localpv=enabled --overwrite
kubectl label nodes fsn-qws-app2 openebs.io/nodeid=fsn-qws-app2
kubectl label nodes fsn-qws-app3 openebs.io/nodeid=fsn-qws-app3
```

# License

All code here is licensed under the EUPL-1.2-or-later.  See the
[LICENSE](./LICENSE) file for details.
