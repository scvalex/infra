{
  config,
  lib,
  pkgs,
  ...
}:

with lib;
let
  cfg = config.ab.machine;
in
{
  imports = [
    ./ab-machine.nix
    ./scvalex-user.nix
  ];

  options = { };

  config = {
    services.openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
      };
      ports = [ 2022 ];
    };

    programs.mosh.enable = true;

    system.stateVersion = "22.05";
  };
}
