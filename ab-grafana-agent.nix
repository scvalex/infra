{
  config,
  lib,
  pkgs,
  ...
}:

with lib;
let
  cfg = config.ab.services.grafana-agent;
  agentConfig = readFile ./secrets/grafana-cloud/agent-config.yaml;
  package = pkgs.grafana-agent;
in
{
  imports = [ ];

  options = {
    ab.services.grafana-agent = {
      enable = mkEnableOption "Grafana Agent for uploading to Grafana Cloud";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.grafana-agent = {
      description = "Scrape metrics and upload them to Grafana Cloud";
      after = [ "network-online.target" ];
      wants = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];

      serviceConfig = {
        ExecStart = "${package}/bin/agent --config.file=${pkgs.writeText "agent-config.yaml" agentConfig}";
        Restart = "always";
      };
      unitConfig = {
        StartLimitIntervalSec = 0;
      };
    };
  };
}
