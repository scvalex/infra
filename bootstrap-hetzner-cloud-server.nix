# See the configuration options in the flake.
{
  system,
  hostName,
  hostId,
  publicInterface,
  nixpkgs,
  disko,
  ...
}:
let
  machineConfig =
    {
      modulesPath,
      config,
      lib,
      pkgs,
      ...
    }:
    {
      imports = [
        (modulesPath + "/installer/scan/not-detected.nix")
        (modulesPath + "/profiles/qemu-guest.nix")
        ./scvalex-user.nix
      ];

      networking.hostName = hostName;
      networking.hostId = hostId;

      environment.systemPackages = map lib.lowPrio [
        pkgs.curl
        pkgs.gitMinimal
      ];

      boot = {
        supportedFilesystems = [ "zfs" ];

        loader = {
          systemd-boot.enable = true;
          efi.canTouchEfiVariables = true;
        };

        kernelParams = [
          "console=tty"
          # See <https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt> for docs on this
          # ip=<client-ip>:<server-ip>:<gw-ip>:<netmask>:<hostname>:<device>:<autoconf>:<dns0-ip>:<dns1-ip>:<ntp0-ip>
          # The server ip refers to the NFS server -- we don't need it.
          "ip=::::${hostName}-initrd:${publicInterface}:dhcp:"
        ];

        initrd = {
          availableKernelModules = [
            "ata_piix"
            "sd_mod"
            "sr_mod"
            "virtio"
            "virtio_net"
            "virtio_pci"
            "virtio_scsi"
          ];
          kernelModules = [ "virtio_gpu" ];

          network = {
            enable = true;
            ssh = {
              enable = true;
              port = 2222;
              # hostKeys paths must be unquoted strings, otherwise you'll run into issues
              # with boot.initrd.secrets the keys are copied to initrd from the path specified;
              # multiple keys can be set you can generate any number of host keys using
              # 'ssh-keygen -t ed25519 -N "" -f /boot/initrd-ssh-key'
              hostKeys = [ /boot/initrd-ssh-key ];
              # public ssh key used for login
              authorizedKeys = [
                "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzJ+BIKXRDhZuX3/kfsVUQ4UoIVzm5lG/TicVWOpDEP2qOBQL1LA5mKRn0CnA7F3Vd+AWJo57NqEsuEw/WY0kzRSQXA3xqmLe2EEi9ZoAEUymHJvfaWXYl/rEcl1FslfczXHW3sKwhsdjHEC4Ikov7UGqsQfmkqNWbiLPLbSSmVso/kPHKG39MI8/QuKCK2NdVtvWm84l4PB7KMrEXNndMMkitgpGVCN9d3XBXSd+pqhdRPydN3y+/Zp9XoZh7aCDPOU9J/P9/4F3noTx3owuD0jk918K6kJihSoQWgXDIkjsN64Bp4tIme3ZcZNfcKHkKQWjoxQAlN9SL8wpasxXJrsxdYz15BR2zESA5/9zLuqSGqN9P71MG3IPepbPpZJucq2nSppkc0qiUJqS4pOWnpENHp+21/dryE2DRvfFe1may8VPzFZ2rzrKBgl6HDioTL01G+SjUnjpT2Mdp2lBs4FmNhOfHLJdpz0ghxIQBVOHZ2CXmW9OKYcyT9PkfTQ06NJD6mjRuwNatV801c5ZC099n04g0pd5rd15t7C1D9CSDUUP4MCjKBbT14pDcNnQdEcdtsZl2qqApbJPrkdGEZ1490bib8SsNfsYN7f5BjOQvtXSkDEQsZ+L9ZYBwSrU/zI6oeZ4jGIiSBDzgkeY/azg2Pl04HMn1VQNWqfMcYQ== ops@scvalex.net"
              ];
            };
            # this will automatically load the zfs password prompt on login
            # and kill the other prompt so boot can continue
            postCommands = ''
              cat <<EOG > /root/.profile
              if pgrep -x "zfs" > /dev/null
              then
                zpool import -f -a || true
                zfs load-key -a
                killall zfs
              else
                echo "zfs not running -- maybe the pool is taking some time to load for some unforseen reason."
              fi
              EOG
            '';
          };
        };
      };

      # Initial empty root password for easy login:
      users.users.root.initialHashedPassword = "";

      users.users.root.openssh.authorizedKeys.keys = [
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzJ+BIKXRDhZuX3/kfsVUQ4UoIVzm5lG/TicVWOpDEP2qOBQL1LA5mKRn0CnA7F3Vd+AWJo57NqEsuEw/WY0kzRSQXA3xqmLe2EEi9ZoAEUymHJvfaWXYl/rEcl1FslfczXHW3sKwhsdjHEC4Ikov7UGqsQfmkqNWbiLPLbSSmVso/kPHKG39MI8/QuKCK2NdVtvWm84l4PB7KMrEXNndMMkitgpGVCN9d3XBXSd+pqhdRPydN3y+/Zp9XoZh7aCDPOU9J/P9/4F3noTx3owuD0jk918K6kJihSoQWgXDIkjsN64Bp4tIme3ZcZNfcKHkKQWjoxQAlN9SL8wpasxXJrsxdYz15BR2zESA5/9zLuqSGqN9P71MG3IPepbPpZJucq2nSppkc0qiUJqS4pOWnpENHp+21/dryE2DRvfFe1may8VPzFZ2rzrKBgl6HDioTL01G+SjUnjpT2Mdp2lBs4FmNhOfHLJdpz0ghxIQBVOHZ2CXmW9OKYcyT9PkfTQ06NJD6mjRuwNatV801c5ZC099n04g0pd5rd15t7C1D9CSDUUP4MCjKBbT14pDcNnQdEcdtsZl2qqApbJPrkdGEZ1490bib8SsNfsYN7f5BjOQvtXSkDEQsZ+L9ZYBwSrU/zI6oeZ4jGIiSBDzgkeY/azg2Pl04HMn1VQNWqfMcYQ== ops@scvalex.net"
      ];
      services.openssh = {
        enable = true;
        settings = {
          PermitRootLogin = "prohibit-password";
          PasswordAuthentication = false;
        };
        ports = [ 2022 ];
      };

      system.stateVersion = "24.11";

      disko.devices = {
        disk = {
          x = {
            type = "disk";
            device = "/dev/sda";
            content = {
              type = "gpt";
              partitions = {
                boot = {
                  name = "boot";
                  size = "1M";
                  type = "EF02";
                };
                ESP = {
                  size = "512M";
                  type = "EF00";
                  label = "BOOT";
                  content = {
                    type = "filesystem";
                    format = "vfat";
                    mountpoint = "/boot";
                  };
                };
                zfs = {
                  size = "100%";
                  content = {
                    type = "zfs";
                    pool = "zroot";
                  };
                };
              };
            };
            postCreateHook = ''
              mkdir -p /boot
              mount /dev/disk/by-partlabel/BOOT /boot
              ssh-keygen -t ed25519 -N "" -f /boot/initrd-ssh-key
              rm /boot/initrd-ssh-key.pub
              umount /boot
            '';
          };
        };
        zpool = {
          zroot = {
            type = "zpool";
            mode = "";
            options = {
              ashift = "12";
            };
            rootFsOptions = {
              mountpoint = "none";
              compression = "zstd";
              atime = "off";
              acltype = "posixacl";
              xattr = "sa";
              encryption = "on";
              keyformat = "passphrase";
              keylocation = "file:///tmp/zpool-encryption.key";
            };
            mountpoint = null;
            postCreateHook = ''
              zfs set keylocation=prompt zroot;
            '';

            datasets = {
              nixos = {
                type = "zfs_fs";
                options.mountpoint = "legacy";
                mountpoint = "/";
              };
              # conduit = {
              #   type = "zfs_fs";
              #   mountpoint = "/var/lib/matrix-conduit";
              # };
              # longhorn-ext4 = {
              #   type = "zfs_volume";
              #   size = "100G";
              #   content = {
              #     type = "filesystem";
              #     format = "ext4";
              #   };
              # };
              # zfs-localpv = {
              #   type = "zfs_fs";
              # };
              # containerd = {
              #   type = "zfs_fs";
              #   mountpoint = "/var/lib/containerd/io.containerd.snapshotter.v1.zfs";
              # };
            };
          };
        };
      };
    };
in
nixpkgs.lib.nixosSystem {
  inherit system;
  modules = [
    disko.nixosModules.disko
    machineConfig
  ];
}
