# fish theme: gentoo

function fish_prompt
    set -l last_status $status
    set fish_greeting
    set -l cyan (set_color -o cyan)
    set -l red (set_color -o red)
    set -l blue (set_color -o blue)
    set -l green (set_color -o green)
    set -l normal (set_color normal)

    set -l cwd (pwd | sed -e "s!^$HOME!~!g")
    # output the prompt, left to right:
    if [ (id -u) = "0" ];
        set cwd (basename $cwd)
        # display host
        echo -n -s $red root @ (uname -n |cut -d . -f 1) " "
    else
        # display 'user@host:'
        echo -n -s $green (whoami) @ $green (uname -n|cut -d . -f 1) " "
    end

    # display the current directory name:
    echo -n -s $blue $cwd $normal

    if test $last_status != 0
        echo -n -s $red ' [' $last_status ']' $normal
    end

    # terminate with a nice prompt char:
    if [ (id -u) = "0" ];
        set indicate '#'
    else
        set indicate '$'
    end
    echo -n -s $blue " $indicate " $normal
end
