{
  config,
  lib,
  pkgs,
  nodes,
  ...
}:

with lib;
let
  cfg = config.ab.services.email;
  password = readFile ./secrets/email/infra-alerts.password;
in
{
  imports = [ ];

  options = {
    ab.services.email = {
      enable = mkOption {
        default = true;
        example = true;
        description = "Whether to enable support for sending email.";
        type = lib.types.bool;
      };

      defaultEmailTo = mkOption {
        type = types.str;
        description = "Default To: address";
        default = "infra-alerts@abstractbinary.org";
      };

      defaultEmailFrom = mkOption {
        type = types.str;
        description = "Default From: address";
        default = "alert-${config.networking.hostName}@mailgun.abstractbinary.org";
      };
    };
  };

  config = mkIf cfg.enable {
    programs.msmtp = {
      enable = true;
      setSendmail = true;
      defaults = {
        aliases = builtins.toFile "aliases" ''
          default: ${cfg.defaultEmailTo}
        '';
      };
      accounts.default = {
        auth = "plain";
        host = "smtp.eu.mailgun.org";
        port = "587";
        user = "infra-alerts@mailgun.abstractbinary.org";
        password = password;
        from = cfg.defaultEmailFrom;
      };
    };
  };
}
