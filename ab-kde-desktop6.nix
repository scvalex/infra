{
  config,
  lib,
  pkgs,
  ...
}:

with lib;
let
in
{
  imports = [ ];

  options = { };

  config = {
    environment.systemPackages =
      with pkgs;
      with kdePackages;
      [
        ark
        easyeffects
        kate
        kdialog
        kitty
        # kmix
        pavucontrol
        pulseaudio

        adwaita-icon-theme
      ];

    programs.dconf.enable = true; # needed by easyeffects

    programs.kdeconnect.enable = true;

    networking.networkmanager.enable = true;

    services.xserver.enable = true;

    services.desktopManager.plasma6.enable = true;
    services.displayManager.sddm.enable = true;
    services.displayManager.sddm.wayland.enable = true;

    services.printing.enable = true;

    services.xserver.xkb.options = "caps:swapescape";

    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      jack.enable = true;
    };

    # Needed to get compose combos with more than two keypresses to
    # work in GTK apps.
    environment.sessionVariables = {
      GTK_IM_MODULE = "xim";
    };

    fonts.packages = with pkgs; [
      atkinson-hyperlegible
      b612
      fira-code
      fira-code-symbols
      iosevka
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-emoji
    ];
  };
}
