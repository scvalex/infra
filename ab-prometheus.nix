{
  config,
  lib,
  pkgs,
  nodes,
  ...
}:
with lib;
let
  cfg = config.ab.services.prometheus;
in
{
  imports = [ ];

  options = {
    ab.services.prometheus = {
      serverEnable = mkEnableOption "Prometheus server";
      clientEnable = mkEnableOption "Prometheus client";
    };
  };

  config = mkMerge [
    (mkIf cfg.serverEnable {
      assertions = [
        {
          assertion = config.ab.services.email.enable;
          message = "Must enable ab.services.email.enable";
        }
      ];

      services.prometheus = {
        enable = true;
        globalConfig.scrape_interval = "10s";
        retentionTime = "7d";
        listenAddress = config.ab.services.wireguard.ipAddress;
        rules = [
          ''
            groups:
            - name: Host health
              rules:
              - alert: LowDiskSpace
                expr: node_filesystem_free_bytes{mountpoint="/"}/1024/1024/1024 < 20
                for: 1m
              - alert: LowMemory
                expr: node_memory_MemAvailable_bytes/1024/1024/1024 < 2
                for: 1m
          ''
        ];
        scrapeConfigs = [
          {
            job_name = "node";
            static_configs = [
              {
                targets = pipe nodes [
                  (filterAttrs (
                    hostname: node:
                    let
                      nodeCfg = node.config.ab.services.prometheus;
                    in
                    nodeCfg.serverEnable || nodeCfg.clientEnable
                  ))
                  (mapAttrsToList (
                    hostname: node: "${hostname}:${toString node.config.services.prometheus.exporters.node.port}"
                  ))
                ];
              }
            ];
          }
        ];
        alertmanager = {
          enable = true;
          listenAddress = config.ab.services.wireguard.ipAddress;
          configuration = {
            receivers = [
              {
                name = "default-receiver";
                email_configs =
                  let
                    msmtpCfg = config.programs.msmtp.accounts.default;
                  in
                  [
                    {
                      to = config.ab.services.email.defaultEmailTo;
                      from = "alert-alertmanager-${config.networking.hostName}@mailgun.abstractbinary.org";
                      smarthost = "${msmtpCfg.host}:${msmtpCfg.port}";
                      require_tls = true;
                      auth_username = msmtpCfg.user;
                      # The password has a \n at the end.  This doesn't
                      # matter to msmtp, but it breaks alertmanager.
                      auth_password = strings.removeSuffix "\n" msmtpCfg.password;
                    }
                  ];
              }
            ];
            route = {
              receiver = "default-receiver";
              group_by = [
                "instance"
                "alertname"
              ];
            };
          };
        };
        alertmanagers = [
          {
            scheme = "http";
            static_configs = [
              {
                targets = pipe nodes [
                  (filterAttrs (
                    hostname: node:
                    let
                      nodeCfg = node.config.ab.services.prometheus;
                    in
                    nodeCfg.serverEnable
                  ))
                  (mapAttrsToList (
                    hostname: node: "${hostname}:${toString node.config.services.prometheus.alertmanager.port}"
                  ))
                ];
              }
            ];
          }
        ];
        extraFlags = [
          # See https://prometheus.io/docs/visualization/consoles/
          "--web.console.templates=${./prometheus-consoles}"
          "--web.console.libraries=${pkgs.prometheus}/etc/prometheus/console_libraries/"
        ];
      };
    })
    (mkIf (cfg.clientEnable || cfg.serverEnable) {
      assertions = [
        {
          assertion = config.ab.services.wireguard.enabled;
          message = "Must enable ab.services.wireguard for Prometheus.";
        }
      ];

      services.prometheus = {
        exporters.node = {
          enable = true;
          listenAddress = config.ab.services.wireguard.ipAddress;
        };
      };
    })
  ];
}
