# Command-line options reference:
# https://kubernetes.io/docs/reference/command-line-tools-reference/kubelet/
#
# Volume provider through longhorn: https://longhorn.io/
#
# Storage:
#   zfs create zroot/containerd -o mountpoint=/var/lib/containerd/io.containerd.snapshotter.v1.zfs
#
#   zfs create zroot/longhorn-ext4 -V 100G
#   mkfs.ext4 /dev/zvol/zdata/longhorn-ext4
#
# Changes to longhorn.yaml:
#   +    create-default-disk-labeled-nodes: true
#         in longhorn-manager DemonSet
#            env:
#   +        - name: PATH
#   +          value: "/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/run/current-system/sw/bin/"
#
# How to add a label to a node after it was initialized:
#
#   kubectl label nodes fsn-qws-app1 node.longhorn.io/create-default-disk=true
#
# Hack to get longhorn engines escaping their pods to work on NixOS (done by a systemd script below):
#   mkdir /usr/local
#   ln -s /run/current-system/sw/bin/ /usr/local/
#
# How to drain/schedule nodes:
#
#   kubectl cordon fsn-qws-app3
#   kubectl uncordon fsn-qws-app3

{
  config,
  lib,
  pkgs,
  nodes,
  ...
}:

with lib;
let
  cfg = config.ab.services.kubernetes;
  machineCfg = config.ab.machine;
  wgCfg = config.ab.services.wireguard;
  netCfg = config.ab.networking;
  kubeMasterIPs = flatten (
    mapAttrsToList (
      _: machine:
      if machine.config.ab.services.kubernetes.master.enable then
        [ machine.config.ab.services.wireguard.ipAddress ]
      else
        [ ]
    ) nodes
  );
  kubeMasterAddress = "kube.eu1";
  etcdEndpoints = flatten (
    mapAttrsToList (
      hostname: machine:
      if machine.config.ab.services.etcd.enable then [ "https://${hostname}.wg:2379" ] else [ ]
    ) nodes
  );
  mkKubeKey = file: {
    text = readFile file;
    user = "kubernetes";
    group = "kubernetes";
  };
  mkKubeSslKey = name: mkKubeKey (./secrets/ssl + "/${name}");
  mkKubeKubeKey = name: mkKubeKey (./secrets/kubernetes + "/${name}");
  kubeSecretsPath = "kubernetes/secret";
  pathToSecret = name: "/etc/${kubeSecretsPath}/${name}";
  kubernetesSecrets = {
    kubernetes_key = mkKubeSslKey "kubernetes-key.pem";
    kubernetes_pem = mkKubeSslKey "kubernetes.pem";
    kubernetes_ca_key = mkKubeSslKey "kubernetes-ca-key.pem";
    kubernetes_ca_pem = mkKubeSslKey "kubernetes-ca.pem";
    kubelet_key = mkKubeSslKey "kubelet-${config.networking.hostName}-key.pem";
    kubelet_pem = mkKubeSslKey "kubelet-${config.networking.hostName}.pem";
    kube_controller_manager_key = mkKubeSslKey "kube-controller-manager-key.pem";
    kube_controller_manager_pem = mkKubeSslKey "kube-controller-manager.pem";
    kubernetes_service_account_key = mkKubeSslKey "kubernetes-service-account-key.pem";
    kubernetes_service_account_pem = mkKubeSslKey "kubernetes-service-account.pem";
    kube_scheduler_key = mkKubeSslKey "kube-scheduler-key.pem";
    kube_scheduler_pem = mkKubeSslKey "kube-scheduler.pem";
    kube_proxy_key = mkKubeSslKey "kube-proxy-key.pem";
    kube_proxy_pem = mkKubeSslKey "kube-proxy.pem";
    kubernetes_admin_key = mkKubeSslKey "kubernetes-admin-key.pem";
    kubernetes_admin_pem = mkKubeSslKey "kubernetes-admin.pem";
    flannel_key = mkKubeSslKey "flannel-key.pem";
    flannel_pem = mkKubeSslKey "flannel.pem";
    kubelet_client_key = mkKubeSslKey "kubelet-client-key.pem";
    kubelet_client_pem = mkKubeSslKey "kubelet-client.pem";
    kube_proxy_client_key = mkKubeSslKey "kube-proxy-client-key.pem";
    kube_proxy_client_pem = mkKubeSslKey "kube-proxy-client.pem";
    kubernetes-encryption-provider-yml = mkKubeKubeKey "encryption-provider.yml";
    etcd_client_key = mkKubeSslKey "etcd-client-${config.networking.hostName}-key.pem";
    etcd_client_pem = mkKubeSslKey "etcd-client-${config.networking.hostName}.pem";
  };
in
{
  imports = [
    ./ab-machine.nix
    ./ab-wireguard.nix
    ./ab-networking.nix
  ];

  options = {
    ab.services.kubernetes = {
      publicIpAddress = mkOption {
        type = types.str;
        default = netCfg.ipAddress;
        description = "Public IP Address";
      };

      privateIpAddress = mkOption {
        type = types.str;
        default = wgCfg.ipAddress;
        description = "Private IP Address";
      };

      privateIface = mkOption {
        type = types.str;
        default = wgCfg.wgInterface;
        description = "Private interface";
      };

      clusterCidrIp4 = mkOption {
        type = types.str;
        default = "10.32.0.0/16";
        description = "Pod IP address range";
      };

      serviceCidrIp4 = mkOption {
        type = types.str;
        default = "10.33.0.0/24";
        description = "Service IP address range";
      };

      master = {
        enable = mkEnableOption "Kubernetes control-plane";
      };

      longhorn = {
        enable = mkEnableOption "Enable longhorn on this node";

        ext4 = mkOption {
          type = types.bool;
          default = false;
          description = "Whether the device is an ext4 partition that should be mounted.";
        };

        device = mkOption {
          type = with types; nullOr str;
          default = null;
          description = "Which device to mount in the longhorn data dir";
        };
      };
    };
  };

  config = mkMerge [
    (mkIf cfg.longhorn.enable {
      assertions = [
        {
          assertion = (!cfg.longhorn.ext4 || cfg.longhorn.device != null);
          message = "cfg.longhorn.device not set for ${config.networking.hostName}";
        }
      ];

      systemd.mounts =
        if cfg.longhorn.ext4 then
          [
            {
              what = cfg.longhorn.device;
              type = "ext4";
              where = "/var/lib/longhorn";
              wantedBy = [ "kubernetes.target" ];
              requiredBy = [ "kubernetes.target" ];
              options = "noatime,discard";
            }
          ]
        else
          [ ];
    })

    (mkIf cfg.master.enable {
      assertions = [
        {
          assertion = machineCfg.encryptedRoot;
          message = "Kubernetes machines must have encrypted roots";
        }
      ];

      services.openiscsi = {
        enable = true;
        # Pretty sure this name doesn't matter.
        name = "${pkgs.openiscsi}/bin/iscsi-iname";
      };

      networking.hosts = pipe kubeMasterIPs [
        (map (ip: nameValuePair ip [ kubeMasterAddress ]))
        listToAttrs
      ];

      networking.firewall.allowedTCPPorts = [
        80
        443
      ];

      # This page claims that these are necessary.
      # https://github.com/kubernetes/kubernetes/blob/c6eedc74a7b4ee27cf69089fcb10d6bc42283027/pkg/proxy/ipvs/README.md
      boot.kernelModules = [
        "ip_vs"
        "ip_vs_rr"
        "ip_vs_wrr"
        "ip_vs_sh"
        "nf_conntrack"
      ];

      boot.kernel.sysctl = {
        "fs.inotify.max_user_instances" = 256;
      };

      environment.systemPackages = with pkgs; [
        ipvsadm
        kubectl
        nfs-utils
        openssl
      ];

      services.kubernetes.path = with pkgs; [ conntrack-tools ];
      systemd.services.kube-proxy.path = with pkgs; [
        ipset
        kmod
      ];

      virtualisation.containerd.settings.plugins."io.containerd.grpc.v1.cri".containerd.snapshotter = "zfs";

      networking.firewall.trustedInterfaces = [ "mynet" ];

      users.users.kubernetes.extraGroups = mkIf (!machineCfg.encryptedRoot) [ "keys" ];

      deployment.keys = mkIf (!machineCfg.encryptedRoot) kubernetesSecrets;

      systemd.services.create-kubernetes-hack-usr-local-symlink = {
        script = ''
          echo "Creating /usr/local/bin symlink for Kubernetes/Longhorn"
          mkdir -p /usr/local
          ln -sf /run/current-system/sw/bin/ /usr/local/
          echo "Symlink created"
        '';
        wantedBy = [ "kubelet.service" ];
        before = [ "kubelet.service" ];
      };

      environment.etc =
        {
          "kubernetes/admin.conf" = {
            source = config.services.kubernetes.lib.mkKubeConfig "cluster-admin" {
              server = config.services.kubernetes.apiserverAddress;
              certFile = pathToSecret "kubernetes_admin_pem";
              keyFile = pathToSecret "kubernetes_admin_key";
            };
          };
        }
        // (mapAttrs' (
          name: value: nameValuePair "${kubeSecretsPath}/${name}" (value // { mode = "0400"; })
        ) kubernetesSecrets);

      services.kubernetes = {
        roles = [
          "master"
          "node"
        ];

        masterAddress = kubeMasterAddress;

        # Does not work for multi-node clusters.
        easyCerts = false;
        pki.enable = false;
        caFile = "/etc/abstractbinary-ca.pem";

        apiserverAddress = "https://${kubeMasterAddress}:6443";

        # Allocate pod IP addresses from these ranges:
        clusterCidr = "${cfg.clusterCidrIp4}";

        # Explicitly say that we're using flannel
        flannel = {
          enable = true;
          openFirewallPorts = false;
        };

        apiserver = {
          advertiseAddress = cfg.privateIpAddress;
          securePort = 6443;
          bindAddress = cfg.privateIpAddress;
          # Allocate service IP addresses from these ranges:
          serviceClusterIpRange = cfg.serviceCidrIp4;
          extraOpts = ''
            \
                        --etcd-prefix=/kube/eu1 \
                        --encryption-provider-config=${pathToSecret "kubernetes-encryption-provider-yml"} \
          '';
          tlsKeyFile = pathToSecret "kubernetes_key";
          tlsCertFile = pathToSecret "kubernetes_pem";
          kubeletClientKeyFile = pathToSecret "kubelet_client_key";
          kubeletClientCertFile = pathToSecret "kubelet_client_pem";
          proxyClientKeyFile = pathToSecret "kube_proxy_client_key";
          proxyClientCertFile = pathToSecret "kube_proxy_client_pem";
          serviceAccountKeyFile = pathToSecret "kubernetes_service_account_key";
          serviceAccountSigningKeyFile = pathToSecret "kubernetes_service_account_key";

          # Needed by longhorn CSI
          allowPrivileged = true;

          etcd = {
            servers = etcdEndpoints;
            keyFile = pathToSecret "etcd_client_key";
            certFile = pathToSecret "etcd_client_pem";
          };
        };

        kubelet = {
          address = cfg.privateIpAddress;
          nodeIp = cfg.privateIpAddress;
          hostname = "${config.networking.hostName}.wg";
          clusterDns = [ config.services.kubernetes.addons.dns.clusterIp ];
          extraOpts = ''
            ${optionalString (cfg.longhorn.enable) "--node-labels node.longhorn.io/create-default-disk=true"} \
          '';
          extraConfig = {
            "failSwapOn" = false;
            "resolvConf" = "/run/systemd/resolve/resolv.conf";
          };
          tlsCertFile = pathToSecret "kubelet_pem";
          tlsKeyFile = pathToSecret "kubelet_key";

          kubeconfig = {
            certFile = pathToSecret "kubelet_pem";
            keyFile = pathToSecret "kubelet_key";
          };
        };

        controllerManager = {
          serviceAccountKeyFile = pathToSecret "kubernetes_service_account_key";
          allocateNodeCIDRs = true;

          extraOpts = ''
            \
                        --service-cluster-ip-range="${cfg.serviceCidrIp4}" \
                        --cluster-signing-cert-file="${pathToSecret "kubernetes_ca_pem"}" \
                        --cluster-signing-key-file="${pathToSecret "kubernetes_ca_key"}" \
          '';
          kubeconfig = {
            certFile = pathToSecret "kube_controller_manager_pem";
            keyFile = pathToSecret "kube_controller_manager_key";
          };
        };

        scheduler = {
          extraOpts = "--bind-address=${cfg.privateIpAddress}";

          kubeconfig = {
            certFile = pathToSecret "kube_scheduler_pem";
            keyFile = pathToSecret "kube_scheduler_key";
          };
        };

        proxy = {
          bindAddress = cfg.privateIpAddress;
          extraOpts = ''
            \
                        --healthz-bind-address=${cfg.privateIpAddress} \
                        --proxy-mode=ipvs \
          '';

          kubeconfig = {
            certFile = pathToSecret "kube_proxy_pem";
            keyFile = pathToSecret "kube_proxy_key";
          };
        };
      };

      systemd.services.kube-addon-manager =
        let
          adminKubeConfig = "/etc/kubernetes/admin.conf";
        in
        {
          environment.KUBECONFIG = adminKubeConfig;

          serviceConfig.PermissionsStartOnly = true;

          preStart =
            with pkgs;
            let
              files = mapAttrsToList (
                n: v: writeText "${n}.json" (builtins.toJSON v)
              ) config.services.kubernetes.addonManager.bootstrapAddons;
            in
            ''
              export KUBECONFIG=${adminKubeConfig}
              ${kubectl}/bin/kubectl apply -f ${concatStringsSep " \\\n -f " files}
            '';
        };

      services.kubernetes.addons.dns.enable = false;

      services.flannel = {
        iface = cfg.privateIface;

        kubeconfig = config.services.kubernetes.lib.mkKubeConfig "flannel" {
          server = "https://${kubeMasterAddress}:6443";
          certFile = pathToSecret "flannel_pem";
          keyFile = pathToSecret "flannel_key";
        };
      };

      systemd.services.flannel = {
        after = [ "sys-devices-virtual-net-wg0.device" ];
        bindsTo = [ "sys-devices-virtual-net-wg0.device" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          Restart = "always";
          TimeoutStopSec = "1";
          TimeoutStopFailureMode = "kill";
        };
        unitConfig = {
          StartLimitIntervalSec = 0;
        };
      };

      # services.udev.extraRules = ''
      #   ACTION=="add", SUBSYSTEM=="net", DEVPATH=="/devices/virtual/net/wg0", TAG+="systemd", RUN+="${startFlannelDelayed}"
      # '';

      systemd.services.kubelet = {
        after = [ "sys-devices-virtual-net-wg0.device" ];
        requires = [ "sys-devices-virtual-net-wg0.device" ];
        # kubelet.service is part of kubernetes.slice which is already
        # wanted by multi-user.target.
        # wantedBy = [ "multi-user.target" ];
        unitConfig = {
          StartLimitIntervalSec = 0;
        };
      };
    })
  ];
}
