{
  config,
  lib,
  pkgs,
  ...
}:

with lib;
let
in
{
  imports = [ ];

  options = { };

  config = {
    environment.systemPackages =
      with pkgs;
      with kdePackages;
      [
        obs-studio
        v4l-utils

        (kdenlive.overrideAttrs (prevAttrs: {
          nativeBuildInputs = (prevAttrs.nativeBuildInputs or [ ]) ++ [ makeBinaryWrapper ];
          qtWrapperArgs =
            let
              gst_plugins_all = with gst_all_1; [
                gstreamer
                gst-libav
                gst-plugins-base
                gst-plugins-good
                gst-plugins-bad
              ];
            in
            (prevAttrs.qtWrapperArgs or [ ])
            ++ [
              "--prefix GST_PLUGIN_PATH : ${lib.makeSearchPath "lib/gstreamer-1.0" gst_plugins_all}"
              "--prefix LADSPA_PATH : ${rnnoise-plugin}/lib/ladspa"
            ];
        }))

      ];

    boot.kernelModules = [ "v4l2loopback" ];
    boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];
  };
}
