#!/bin/sh

set -e

export MANIFEST_URL=https://raw.githubusercontent.com/grafana/agent/main/production/kubernetes/agent-bare.yaml
export NAMESPACE=monitoring
curl -fsSL https://raw.githubusercontent.com/grafana/agent/release/production/kubernetes/install-bare.sh | sh > grafana-agent.yaml
