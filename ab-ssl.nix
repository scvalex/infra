{
  config,
  lib,
  pkgs,
  ...
}:

with lib;
{
  imports = [ ];

  options = { };

  config = {
    environment.etc."abstractbinary-ca.pem" = {
      source = ./secrets/ssl/abstractbinary-ca.pem;
      mode = "0644";
    };
  };
}
