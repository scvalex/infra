{
  config,
  lib,
  pkgs,
  ...
}:

with lib;
let
  cfg = config.ab.healthchecks;
in
{
  imports = [ ];

  options = {
    ab.healthchecks.enable = mkEnableOption "HealthChecks.io monitoring";

    ab.healthchecks.url = mkOption {
      type = types.str;
      description = "URL of HelathChecks.io check";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.ping-healthchecks = {
      serviceConfig = {
        Type = "oneshot";
        ExecStart = "${pkgs.curl}/bin/curl ${cfg.url}";
      };
      startAt = "*-*-* *:03/15:00";
    };
  };
}
