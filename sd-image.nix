# nix-build ./nix-channel/nixpkgs/nixos -A config.system.build.sdImage -I nixos-config=./sd-image.nix --argstr system aarch64-linux

{ ... }:
{
  imports = [
    <nixpkgs/nixos/modules/installer/sd-card/sd-image-aarch64.nix>
    ./ab-server.nix
  ];

  ab.machine.class = "pi_server";
  ab.machine.populateHosts = false;

  ab.healthchecks.url = "https://hc-ping.com/9c39c262-6638-43c9-8ae4-ae2b596d150b";

  networking.hostName = "edb-qws-pi2";
  networking.hostId = "e541c288";
}
