{
  name,
  config,
  modulesPath,
  pkgs,
  lib,
  machines,
  ...
}:
with lib;
let
  hostname = name;
  machine = machines.${hostname};
in
{
  deployment.targetHost = null;

  ab.networking = {
    ipAddress = if (machine ? ipAddress) then machine.ipAddress else null;
    ip6Address = if (machine ? ip6Address) then machine.ip6Address else null;
  };

  ab.services.wireguard = mkMerge [
    { enabled = false; }
    (mkIf (machine ? wireguard) (
      with machine.wireguard;
      {
        inherit publicKey ipAddress ip6Address;
        enabled = false;
      }
    ))
  ];
}
