{
  name,
  config,
  modulesPath,
  pkgs,
  lib,
  nodes,
  machines,
  nixpkgs-unstable,
  nixos-hardware,
  ldgr,
  kb-classic,
  picotron,
  ...
}:
with lib;
let
  hostname = name;
  machine = machines.${hostname};
  serviceAliases = {
    "syncthing" = {
      alias = "10.20.0.2";
      real = "10.33.0.235:8384";
    };
    "warrior" = {
      alias = "10.20.0.3";
      real = "10.33.0.193:8001";
    };
    "syncthing-local" = {
      alias = "10.20.0.6";
      real = "127.0.0.1:8384";
    };
    "livebook" = {
      alias = "10.20.0.7";
      real = "127.0.0.1:30123";
    };
  };
  myEmacs = (pkgs.emacsPackagesFor pkgs.emacs30).emacsWithPackages (
    epkgs: with epkgs; [ treesit-grammars.with-all-grammars ]
  );
in
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    nixos-hardware.nixosModules.common-pc-ssd
    nixos-hardware.nixosModules.common-pc-laptop
    ../ab-machine.nix
    ../ab-kde-desktop6.nix
    ../ab-obs.nix
  ];

  system.stateVersion = "22.05";

  networking.hostName = hostname;
  networking.hostId = "9a5d5717";

  services.irqbalance.enable = true;

  deployment = {
    allowLocalDeployment = true;
    targetHost = null;
    tags = [ "real" ];
  };

  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "nvme"
    "usbhid"
    "usb_storage"
    "sd_mod"
    "xe"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelPackages = pkgs.linuxPackages_6_12;
  boot.kernelModules = [ "kvm-intel" ];
  boot.supportedFilesystems = [ "zfs" ];
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # ARM64 builder support
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  # Distributed builds also require root to have the right ssh key
  # without a password and the below in its .ssh/config
  #
  # Match user nixdist
  #   IdentityFile ~/.ssh/nixdist
  #   StrictHostKeyChecking=accept-new
  #   Port 2022
  #
  # Test with:
  # nix store ping --store ssh-ng://nixdist@nur-aws-vpn5
  #
  nix.distributedBuilds = true;
  nix.buildMachines = [
    {
      hostName = "nur-aws-vpn5";
      system = "aarch64-linux";
      maxJobs = 1;
      speedFactor = 4;
      sshUser = "nixdist";
      protocol = "ssh-ng";
      sshKey = "/root/.ssh/nixdist";
      supportedFeatures = [
        "big-parallel"
        "kvm"
      ];
    }
    {
      hostName = "cibo.lan";
      system = "aarch64-linux";
      maxJobs = 1;
      speedFactor = 2;
      sshUser = "nixdist";
      protocol = "ssh-ng";
      sshKey = "/root/.ssh/nixdist";
      supportedFeatures = [
        "big-parallel"
        "kvm"
      ];
    }
    {
      hostName = "cibo.lan";
      system = "x86_64-linux";
      maxJobs = 1;
      speedFactor = 4;
      sshUser = "nixdist";
      protocol = "ssh-ng";
      sshKey = "/root/.ssh/nixdist";
      supportedFeatures = [
        "big-parallel"
        "kvm"
      ];
    }
  ];

  services.zfs.trim.enable = true;
  services.zfs.autoScrub.enable = true;

  fileSystems."/" = {
    device = "zroot/nixos";
    fsType = "zfs";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/33D9-CCC2";
    fsType = "vfat";
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  hardware.bluetooth.enable = true;

  services.fwupd.enable = true;

  ab.machine = {
    isWorkstation = true;
    encryptedRoot = true;
    zpools = [ "zroot" ];
  };

  networking = {
    useDHCP = false;
    interfaces.enp0s31f6.useDHCP = true;
    interfaces.wlp3s0.useDHCP = true;
  };

  ab.networking = {
    lan = machine.lan;
  };

  ab.services.wireguard = with machine.wireguard; {
    inherit
      publicKey
      privateKey
      ipAddress
      ip6Address
      ;
    routeEverythingThroughWg = true;
    routeThroughPeer = {
      "nur-aws-vpn5" = [
        "0.0.0.0/0"
        "::/0"
      ];
      "fsn-qpr-kube1" = [ "10.33.0.0/24" ];
      "fsn-qpr-kube2" = [ "10.33.0.0/24" ];
      "fsn-qpr-kube3" = [ "10.33.0.0/24" ];
    };
  };

  # Temp ssh access
  # services.openssh = {
  #   enable = true;
  #   settings = {
  #     PasswordAuthentication = false;
  #   };
  #   ports = [ 2022 ];
  # };
  # networking.firewall.allowedTCPPorts = [ 2022 ];

  # iwd is theoretically better than wpa_supplicant, but it adds
  # 1m30s of stall to my boots.  Maybe the next laptop will
  # support it…
  # networking.networkmanager.wifi.backend = "iwd";

  time.timeZone = "Europe/London";
  i18n.defaultLocale = "en_GB.UTF-8";

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      intel-media-driver
      intel-media-sdk
      vaapiIntel
    ];
  };

  hardware.rtl-sdr.enable = true;

  nixpkgs.config.allowUnfree = true;

  environment.systemPackages =
    with pkgs;
    with kdePackages;
    [
      aseprite
      aspell
      aspellDicts.en
      aspellDicts.en-computers
      bluez-tools
      broot
      cargo
      cargo-generate
      chromium
      discord
      element-desktop
      ffmpeg-normalize
      ffmpeg_7-full
      firefox
      git-crypt
      gnumake
      godot_4
      google-cloud-sdk
      inkscape
      keepassxc
      krew
      krita
      kubectl
      labplot
      python312Packages.meshtastic
      nix-diff
      nixfmt-rfc-style
      nvtopPackages.intel
      openscad
      postgresql_15
      puddletag
      python3
      signal-desktop
      skypeforlinux
      starship
      syncthing
      tenacity
      thunderbird
      tor-browser-bundle-bin
      traceroute
      unrar
      vlc
      yt-dlp
      zellij
      zoom-us
      zotero

      myEmacs

      nixpkgs-unstable.sdrpp

      blender
      # nixpkgs-unstable.blender
      nixpkgs-unstable.onlyoffice-bin_latest
      nixpkgs-unstable.cura-appimage

      (pkgs.buildFHSUserEnv {
        name = "zed";
        targetPkgs = pkgs: with pkgs; [ nixpkgs-unstable.zed-editor ];
        runScript = "zed";
      })

      ldgr.defaultPackage."${config.nixpkgs.system}"
      kb-classic.defaultPackage."${config.nixpkgs.system}"
      picotron.packages."${config.nixpkgs.system}".default
    ];

  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
    extraPackages = [ pkgs.zfs ];
  };

  # virtualisation.virtualbox.host.enable = true;
  # users.extraGroups.vboxusers.members = [ "scvalex" ];

  services.udev.extraRules = ''
    SUBSYSTEM=="tty", ATTRS{manufacturer}=="Arduino*", MODE:="666"
    SUBSYSTEM=="tty", ATTRS{product}=="BBC micro:bit*", MODE:="666"
    ATTRS{product}=="*CMSIS-DAP*", MODE="666", TAG+="uaccess"
    SUBSYSTEM=="hidraw", ATTRS{product}=="BBC micro:bit*", MODE:="666"
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0bda", ATTRS{idProduct}=="2838", MODE:="666"
    # Pico W
    SUBSYSTEMS=="usb", ATTRS{idVendor}=="2e8a", ATTRS{idProduct}=="f00a", MODE:="666"
  '';

  programs.gnupg.agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-gtk2;
  };
  programs.ssh.startAgent = true;

  programs.steam.enable = true;

  programs.direnv.enable = true;

  services.emacs = {
    package = myEmacs;
    enable = true;
    startWithGraphical = true;
  };

  services.livebook = {
    enableUserService = true;
    package = nixpkgs-unstable.livebook;
    environmentFile = "/home/scvalex/repo/infra/secrets/passwords/livebook";
    extraPackages = with pkgs; [
      gnumake
      gcc
      git
    ];
  };

  systemd.user.services.postgresql = {
    serviceConfig = {
      Type = "forking";
      Restart = "always";
      ExecStart = ''
        \
                    ${pkgs.postgresql_15}/bin/pg_ctl start -w -o "-k /tmp -p 5432" -D /home/scvalex/db
      '';
      ExecStop = ''
        \
                    ${pkgs.postgresql_15}/bin/pg_ctl stop -m fast -D /home/scvalex/db
      '';
    };
    path = [ ];
    wantedBy = [ "default.target" ];
  };

  # Needed for Skype.
  services.flatpak.enable = true;

  # services.ollama = {
  #   package = nixpkgs-unstable.ollama;
  #   enable = true;
  # };

  users.users.scvalex = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "networkmanager"
      "audio"
    ];
  };

  ab.services.smartd.enable = true;
  programs.nbd.enable = true;

  # The HP printer still needs to be setup manually with:
  # # NIXPKGS_ALLOW_UNFREE=1 nix shell --impure nixpkgs#hplipWithPlugin
  # # hp-setup
  ### Specify IP address of the printer in advanced options
  services.printing.enable = true;
  services.printing.drivers = [ pkgs.hplipWithPlugin ];

  systemd.user.services.git-commit-notes = {
    serviceConfig = {
      Type = "oneshot";
      WorkingDirectory = "/home/scvalex/Documents/notes/";
    };
    startAt = "*-*-* *:15:00";
    script = ''
      if [[ -n "$(${pkgs.git}/bin/git status --porcelain)" ]]; then
        ${pkgs.git}/bin/git add --all
        ${pkgs.git}/bin/git commit -am "cron: hourly commit"
      else
        echo "No changes to commit"
      fi
    '';
  };

  services.syncthing = {
    enable = true;
    user = "scvalex";
    configDir = "/home/scvalex/.config/syncthing";
    dataDir = "/home/scvalex/";
    overrideFolders = false;
    overrideDevices = false;
  };

  networking.ipvs = {
    enable = true;
    services = mapAttrs' (
      name:
      { alias, real }:
      nameValuePair name {
        address = "${alias}:80";
        servers = [ { address = real; } ];
      }
    ) serviceAliases;
  };

  networking.hosts = zipAttrsWith (_: values: concatLists values) [
    (mapAttrs' (
      _: machine: nameValuePair machine.config.ab.services.wireguard.ipAddress [ "kube.eu1" ]
    ) (filterAttrs (hostname: machine: machine.config.ab.services.kubernetes.master.enable) nodes))
    (mapAttrs' (name: { alias, ... }: nameValuePair alias [ name ]) serviceAliases)
    {
      "0.0.0.0" = [
        "9gag.com"
        "news.ycombinator.com"
        "x.com"
        "twitter.com"
      ];
      "10.33.0.41" = [ "longhorn-ui" ];
    }
  ];
}
