{
  name,
  config,
  modulesPath,
  pkgs,
  lib,
  nodes,
  machines,
  nixos-hardware,
  ...
}:
with lib;
let
  hostname = name;
  machine = machines.${hostname};
in
{
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    nixos-hardware.nixosModules.common-cpu-amd
    nixos-hardware.nixosModules.common-gpu-amd
    nixos-hardware.nixosModules.common-gpu-amd-southern-islands
    nixos-hardware.nixosModules.common-gpu-nvidia-nonprime
    nixos-hardware.nixosModules.common-pc-ssd
    ../ab-server.nix
    ../ab-kde-desktop6.nix
    ../ab-obs.nix
  ];

  networking.hostName = hostname;
  networking.hostId = "34e3e678";

  deployment = {
    targetHost = "${hostname}.lan";
    targetPort = 2022;
    tags = [ "real" ];
  };

  boot.initrd.availableKernelModules = [
    "xhci_pci"
    "ahci"
    "usbhid"
    "usb_storage"
    "sd_mod"
  ];
  boot.kernelPackages = pkgs.linuxPackages_6_12;
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  boot.supportedFilesystems = [ "zfs" ];
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # boot.kernelPatches = [
  #   {
  #     name = "RTL8125D support";
  #     patch = ./8125d-backported.patch;
  #   }
  # ];

  # ARM64 builder support
  boot.binfmt.emulatedSystems = [ "aarch64-linux" ];

  services.zfs.trim.enable = true;
  services.zfs.autoScrub.enable = true;

  fileSystems."/" = {
    device = "zroot/nixos";
    fsType = "zfs";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/1868-26A4";
    fsType = "vfat";
  };

  fileSystems."/home" = {
    device = "zroot/nixos/home";
    fsType = "zfs";
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
  hardware.bluetooth.enable = true;

  hardware.nvidia = {
    powerManagement.enable = false;
    powerManagement.finegrained = false;
    open = false;
    nvidiaSettings = true;
    package = config.boot.kernelPackages.nvidiaPackages.latest;
  };

  hardware.graphics = {
    enable = true;
    enable32Bit = true;
    extraPackages = with pkgs; [
      intel-media-driver
      vaapiIntel
    ];
  };

  services.fwupd.enable = true;

  ab.machine = {
    isWorkstation = true;
    encryptedRoot = true;
    zpools = [ "zroot" ];
  };

  networking = {
    useDHCP = false;
    interfaces.enp8s0u5.useDHCP = true;
    firewall.trustedInterfaces = [ machine.lanInterface ];
  };

  ab.networking = {
    lan = machine.lan;
    lanIpAddress = machine.lanIpAddress;
  };

  ab.services.wireguard = with machine.wireguard; {
    inherit
      publicKey
      privateKey
      ipAddress
      ip6Address
      ;
    routeEverythingThroughWg = true;
    routeThroughPeer = {
      "nur-aws-vpn5" = [
        "0.0.0.0/0"
        "::/0"
      ];
      "fsn-qpr-kube1" = [ "10.33.0.0/24" ];
      "fsn-qpr-kube2" = [ "10.33.0.0/24" ];
      "fsn-qpr-kube3" = [ "10.33.0.0/24" ];
    };
  };
  systemd.network.networks."50-wg0".linkConfig.ActivationPolicy = "manual";

  time.timeZone = "Europe/London";
  i18n.defaultLocale = "en_GB.UTF-8";

  nixpkgs.config.allowUnfree = true;
  nixpkgs.config.permittedInsecurePackages = [ "olm-3.2.16" ];

  environment.systemPackages =
    with pkgs;
    with kdePackages;
    [
      aspell
      aspellDicts.en
      aspellDicts.en-computers
      chromium
      discord
      element-desktop
      emacs
      firefox
      gnumake
      google-cloud-sdk
      inkscape
      krita
      kubectl
      mumble
      neochat
      nix-diff
      nixfmt-rfc-style
      nvtopPackages.full
      onlyoffice-bin
      python3
      signal-desktop
      starship
      syncthing
      thunderbird
      tor-browser-bundle-bin
      traceroute
      zellij

      (blender.override { cudaSupport = true; })
    ];

  services.flatpak.enable = true;

  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
    extraPackages = [ pkgs.zfs ];
  };

  programs.gnupg.agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-gtk2;
  };

  programs.direnv.enable = true;

  users.users.scvalex = {
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "networkmanager"
      "audio"
    ];
  };

  ab.services.smartd.enable = true;
  programs.nbd.enable = true;
  programs.steam.enable = true;

  ab.services.build-machine.enable = true;

  # The HP printer still needs to be setup manually with:
  # # NIXPKGS_ALLOW_UNFREE=1 nix shell --impure nixpkgs#hplipWithPlugin
  # # hp-setup
  ### Specify IP address of the printer in advanced options
  services.printing.enable = true;
  # services.printing.drivers = [ pkgs.hplipWithPlugin ];

  networking.hosts = zipAttrsWith (_: values: concatLists values) [
    (mapAttrs' (
      _: machine: nameValuePair machine.config.ab.services.wireguard.ipAddress [ "kube.eu1" ]
    ) (filterAttrs (_: machine: machine.config.ab.services.kubernetes.master.enable) nodes))
    { "0.0.0.0" = [ "9gag.com" ]; }
  ];
}
