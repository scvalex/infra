{
  name,
  config,
  modulesPath,
  lib,
  machines,
  ...
}:
with lib;
let
  hostname = name;
  machine = machines.${hostname};
in
{
  imports = [ ../ab-hetzner-cloud-server-zfs.nix ];

  nixpkgs.system = "aarch64-linux";

  networking.hostName = hostname;
  networking.hostId = "88789f95";

  deployment = {
    targetHost = "${hostname}.external";
    # targetHost = "167.235.30.149";
    targetPort = 2022;
    tags = [ "real" ];
  };

  ab.networking = with machine; {
    enable = true;
    vpn = true;
    inherit publicInterface ipAddress ip6Address;
  };

  ab.machine = {
    singleBootDevice = "/dev/sda";
    preferSystemdBoot = true;
  };

  ab.services.wireguard = with machine.wireguard; {
    inherit
      publicKey
      privateKey
      ipAddress
      ip6Address
      ;

    routeThroughPeer = {
      "fsn-qpr-kube1" = [ "10.33.0.0/24" ];
      "fsn-qpr-kube2" = [ "10.33.0.0/24" ];
      "fsn-qpr-kube3" = [ "10.33.0.0/24" ];
    };
  };

  ab.services.build-machine.enable = true;

  services.nbd = {
    server = {
      enable = true;
      exports.vault-nur1 = {
        path = "/dev/disk/by-id/scsi-0HC_Volume_17235401";
        allowAddresses = [ "${machine.wireguard.ipAddress}/24" ];
      };
      listenAddress = machine.wireguard.ipAddress;
      listenPort = 30123;
    };
  };
  systemd.services.nbd-server.serviceConfig.Restart = lib.mkForce "always";
  systemd.services.nbd-server.unitConfig.startLimitIntervalSec = 0;

  ab.services.prometheus = {
    clientEnable = true;
    serverEnable = true;
  };
}
