{ lib, ... }:
with lib;
{
  imports = [ ./fsn-qpr-kube-gen.nix ];

  networking.hostId = "296d7089";

  ab.services.zfs-backup-sink = {
    enable = true;
    sshKey = ./secrets/ssh/syncoid;
    pairs = {
      blog-ghost = {
        source = "syncoid@157.90.166.239:zroot/ghost";
        target = "zroot/backup/forth-investing/ghost";
      };
      blog-mysql = {
        source = "syncoid@157.90.166.239:zroot/mysql";
        target = "zroot/backup/forth-investing/mysql";
      };
    };
  };

  services.borgbackup.repos.borgrepo = {
    path = "/var/lib/borgrepo";
    authorizedKeys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFdutMYMQuS8QI0wVpthxg0Snha5LiSnD2ccKBafVeOl borg"
    ];
    allowSubRepos = true;
  };

  services.sanoid = {
    enable = true;
    interval = "00:30";
    templates.prod-backup = {
      hourly = 4;
      daily = 30;
      monthly = 4;
      autosnap = true;
      autoprune = true;
    };
    datasets = {
      "zroot/backup/borgrepo".useTemplate = [ "prod-backup" ];
    };
  };
}
