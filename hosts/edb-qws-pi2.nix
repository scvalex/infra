{
  name,
  config,
  modulesPath,
  pkgs,
  lib,
  machines,
  nixos-hardware,
  ...
}:
with lib;
let
  hostname = name;
  machine = machines.${hostname};
in
{
  imports = [
    ../ab-server.nix
    nixos-hardware.nixosModules.raspberry-pi-4
  ];

  networking = {
    firewall.trustedInterfaces = [ machine.lanInterface ];
  };

  ab.networking = {
    lan = machine.lan;
    lanIpAddress = machine.lanIpAddress;
  };

  ab.services.wireguard = with machine.wireguard; {
    inherit
      publicKey
      privateKey
      ipAddress
      ip6Address
      ;
  };

  networking.hostName = hostname;
  networking.hostId = "f2a451fb";

  time.timeZone = "Europe/London";

  deployment = {
    targetHost = "${hostname}.lan";
    targetPort = 2022;
    tags = [ "real" ];
  };

  nixpkgs.system = "aarch64-linux";

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };

  environment.systemPackages = with pkgs; [
    libraspberrypi
    raspberrypi-eeprom
  ];

  ab.healthchecks = {
    enable = true;
    url = "https://hc-ping.com/9c39c262-6638-43c9-8ae4-ae2b596d150b";
  };

  ab.services.pihole.enable = true;

  documentation.man.enable = false;

  # ab.services.iot-hub.enable = true;
}
