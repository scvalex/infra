{ lib, ... }:
with lib;
{
  imports = [ ./fsn-qpr-kube-gen.nix ];

  networking.hostId = "3c1c9d56";
}
