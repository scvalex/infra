{
  name,
  config,
  modulesPath,
  lib,
  machines,
  ...
}:
with lib;
let
  hostname = name;
  machine = machines.${hostname};
  readSecret = file: replaceStrings [ "\n" ] [ "" ] (readFile file);
in
{
  imports = [ ../ab-hetzner-cloud-server-zfs.nix ];

  nixpkgs.system = "aarch64-linux";

  networking.hostName = hostname;
  networking.hostId = "cadf75af";

  deployment = {
    targetHost = "${hostname}.external";
    targetPort = 2022;
    tags = [ "real" ];
  };

  networking.firewall.allowedTCPPorts = [
    # conduit
    80
    443
    8448
    # coturn
    3478
    5349
  ];
  networking.firewall.allowedUDPPorts = [
    # coturn
    3478
    5349
  ];
  networking.firewall.allowedUDPPortRanges = [
    # coturn
    {
      from = 49152;
      to = 65535;
    }
  ];

  ab.networking = with machine; {
    enable = true;
    vpn = true;
    inherit publicInterface ipAddress ip6Address;
  };

  ab.machine = {
    singleBootDevice = "/dev/sda";
    preferSystemdBoot = true;
  };

  ab.services.wireguard = with machine.wireguard; {
    inherit
      publicKey
      privateKey
      ipAddress
      ip6Address
      ;

    routeThroughPeer = {
      "fsn-qpr-kube1" = [ "10.33.0.0/24" ];
      "fsn-qpr-kube2" = [ "10.33.0.0/24" ];
      "fsn-qpr-kube3" = [ "10.33.0.0/24" ];
    };
  };

  ab.services.prometheus = {
    clientEnable = true;
  };

  services.matrix-conduit = {
    enable = true;
    settings.global = {
      server_name = "abstractbinary.org";
      database_backend = "rocksdb";
      allow_registration = true;
      registration_token = readSecret ../secrets/passwords/matrix-registration-token.key;
      well_known_client = "https://matrix.abstractbinary.org";
      well_known_server = "matrix.abstractbinary.org:443";
      turn_uris = [
        "turn:matrix.abstractbinary.org?transport=udp"
        "turn:matrix.abstractbinary.org?transport=tcp"
      ];
      turn_secret = readFile ../secrets/passwords/coturn.key;
    };
  };

  security.acme = {
    acceptTerms = true;
    defaults.email = "ssl@abstractbinary.org";
  };

  services.nginx = {
    enable = true;
    clientMaxBodySize = "40M";
    recommendedProxySettings = true;
    virtualHosts."matrix.abstractbinary.org" = {
      forceSSL = true;
      enableACME = true;
      listen = [
        {
          addr = "0.0.0.0";
          port = 80;
        }
        {
          addr = "0.0.0.0";
          port = 443;
          ssl = true;
        }
        {
          addr = "[::]";
          port = 80;
        }
        {
          addr = "[::]";
          port = 443;
          ssl = true;
        }
        {
          addr = "0.0.0.0";
          port = 8448;
          ssl = true;
        }
        {
          addr = "[::]";
          port = 8448;
          ssl = true;
        }
      ];
      locations."/_matrix/" = {
        proxyPass = "http://[::1]:6167";
        proxyWebsockets = true;
      };
      locations."/.well-known/matrix/" = {
        proxyPass = "http://[::1]:6167";
        proxyWebsockets = true;
      };
      locations."/" = {
        return = "404";
      };
    };
  };

  services.coturn = {
    enable = true;
    realm = "matrix.abstractbinary.org";
    use-auth-secret = true;
    static-auth-secret = readFile ../secrets/passwords/coturn.key;
  };

  environment.etc."borg/borg-ssh-key" = {
    text = readFile ../secrets/ssh/borg;
    user = "root";
    group = "root";
    mode = "0400";
  };

  services.borgbackup.jobs.matrix-conduit = {
    paths = "/var/lib/matrix-conduit";
    repo = "borg@fsn-qpr-kube2.wg:/var/lib/borgrepo/matrix-conduit";
    encryption.mode = "none";
    compression = "auto,zstd";
    startAt = "daily";
    environment.BORG_RSH = "ssh -p 2022 -i /etc/borg/borg-ssh-key";
    prune.keep = {
      within = "1d"; # Keep all archives from the last day
      daily = 7;
      weekly = 4;
      monthly = 3;
    };
  };
}
