{ lib, ... }:
with lib;
{
  imports = [ ./fsn-qpr-kube-gen.nix ];

  networking.hostId = "7e1fffd9";
}
