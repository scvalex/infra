{
  name,
  config,
  modulesPath,
  lib,
  machines,
  ...
}:
with lib;
let
  hostname = name;
  machine = machines.${hostname};
in
{
  imports = [ ../ab-hetzner-cloud-server-zfs.nix ];

  networking.hostName = hostname;

  deployment = {
    targetHost = "${hostname}.external";
    targetPort = 2022;
    tags = [ "real" ];
  };

  # For foundryvtt's ftp server
  networking.firewall.allowedTCPPorts = [ 31115 ];

  ab.networking = with machine; {
    enable = true;
    inherit publicInterface ipAddress ip6Address;
  };

  ab.machine = {
    singleBootDevice = "/dev/sda";
    zpools = [ "zroot" ];
  };

  ab.services.wireguard = with machine.wireguard; {
    inherit
      publicKey
      privateKey
      ipAddress
      ip6Address
      ;
  };

  ab.services.etcd.enable = true;

  ab.services.kubernetes = {
    master.enable = true;
    longhorn = {
      enable = true;
      ext4 = true;
      device = "/dev/zvol/zroot/longhorn-ext4";
    };
  };

  ab.services.prometheus.clientEnable = true;
}
