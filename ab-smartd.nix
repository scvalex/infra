{
  config,
  modulesPath,
  lib,
  pkgs,
  ...
}:

with lib;
let
  cfg = config.ab.services.smartd;
  emailCfg = config.ab.services.email;
in
{
  imports = [ ./ab-email.nix ];

  options = {
    ab.services.smartd = {
      enable = mkEnableOption "SMART disk health monitoring";
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [ smartmontools ];

    services.smartd.enable = true;

    services.smartd.notifications.mail = mkIf emailCfg.enable {
      enable = true;
      sender = emailCfg.defaultEmailFrom;
      recipient = emailCfg.defaultEmailTo;
    };
  };
}
