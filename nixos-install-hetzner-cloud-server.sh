#! /usr/bin/env bash
#
# Notes to install NixOS on Hetzner Cloud using the recovery system.

set -e

# Inspect existing disks
lsblk

# NAME    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
# sda       8:0    0 19.1G  0 disk
# ├─sda1    8:1    0   19G  0 part /
# ├─sda14   8:14   0    1M  0 part
# └─sda15   8:15   0  122M  0 part /boot/efi
# sr0      11:0    1 1024M  0 rom

# Add zfs repos
cat > /etc/apt/sources.list.d/bullseye-backports.list <<EOF
deb http://deb.debian.org/debian bullseye-backports main contrib non-free
deb-src http://deb.debian.org/debian bullseye-backports main contrib non-free
EOF

cat > /etc/apt/preferences.d/90_zfs <<EOF
Package: libnvpair1linux libnvpair3linux libuutil1linux libuutil3linux libzfs2linux libzfs4linux libzpool2linux libzpool4linux spl-dkms zfs-dkms zfs-test zfsutils-linux zfsutils-linux-dev zfs-zed
Pin: release n=bullseye-backports
Pin-Priority: 990
EOF

apt update
apt install -y dpkg-dev linux-headers-$(uname -r) linux-image-amd64 lshw sudo parted
apt install -y zfs-dkms zfsutils-linux

# Delete the hetzner warning about zfs
rm /usr/local/sbin/zfs /usr/local/sbin/zpool
hash -r

# Undo existing setups to allow running the script multiple times to iterate on it.
# We allow these operations to fail for the case the script runs the first time.
set +e
umount /mnt
set -e

# Create partition tables (--script to not ask)
for d in sda; do
# for d in nvme0n1 nvme1n1; do
    echo "Preparing disk ${d}"
    parted --script --align optimal "/dev/${d}" -- mklabel gpt mkpart 'BIOS-boot' 1MB 2MB set 1 bios_grub on mkpart 'boot' 2MB 512MB mkpart 'zfs-pool' 512MB '100%'
    partprobe
    sleep 1
    mkfs.vfat "/dev/${d}2"
    # mkfs.vfat "/dev/${d}p2"
done

fatlabel /dev/sda2 BOOT-1

# Setup zfs pools

ls -lh /dev/disk/by-id/

# lrwxrwxrwx 1 root root  9 Dec 14 19:45 ata-QEMU_DVD-ROM_QM00003 -> ../../sr0
# lrwxrwxrwx 1 root root  9 Dec 14 19:56 scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-0-0-0 -> ../../sda
# lrwxrwxrwx 1 root root 10 Dec 14 19:57 scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-0-0-0-part1 -> ../../sda1
# lrwxrwxrwx 1 root root 10 Dec 14 19:57 scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-0-0-0-part2 -> ../../sda2
# lrwxrwxrwx 1 root root 10 Dec 14 19:56 scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-0-0-0-part3 -> ../../sda3

zpool create \
    -O mountpoint=none -o ashift=12 -O atime=off -O acltype=posixacl -O xattr=sa -O compression=zstd \
    -O encryption=aes-256-gcm -O keyformat=passphrase \
    zroot \
    scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-0-0-0-part3

# Mount everything

zfs create -o mountpoint=legacy zroot/nixos
mount -t zfs zroot/nixos /mnt

for i in 1; do
# for i in 1 2; do
    mkdir "/mnt/boot-${i}"
done

mount /dev/sda2 /mnt/boot-1

ssh-keygen -t ed25519 -N "" -f /mnt/boot-1/initrd-ssh-key
rm /mnt/boot-1/initrd-ssh-key.pub

# for i in 2 3 4; do
# # for i in 2; do
#     cp /mnt/boot-1/initrd-ssh-key "/mnt/boot-${i}/initrd-ssh-key"
# done

# Installing nix

# Allow installing nix as root, see
#   https://github.com/NixOS/nix/issues/936#issuecomment-475795730
mkdir -p /etc/nix
echo -e "experimental-features = auto-allocate-uids\nauto-allocate-uids = true" > /etc/nix/nix.conf

zfs create -o mountpoint=legacy zroot/tmp-install
mkdir /nix
mount -t zfs zroot/tmp-install /nix

curl -L https://nixos.org/nix/install | sh
set +u +x # sourcing this may refer to unset variables that we have no control over
. $HOME/.nix-profile/etc/profile.d/nix.sh
set -u -x

# Keep in sync with `system.stateVersion` set below!
nix-channel --add https://nixos.org/channels/nixos-22.05 nixpkgs
nix-channel --update

# Getting NixOS installation tools
nix-env -iE "_: with import <nixpkgs/nixos> { configuration = {}; }; with config.system.build; [ nixos-generate-config nixos-install nixos-enter manual.manpages ]"

nixos-generate-config --root /mnt

# Find the name of the network interface that connects us to the Internet.
# Inspired by https://unix.stackexchange.com/questions/14961/how-to-find-out-which-interface-am-i-using-for-connecting-to-the-internet/302613#302613
RESCUE_INTERFACE=$(ip route get 8.8.8.8 | grep -Po '(?<=dev )(\S+)')

# Find what its name will be under NixOS, which uses stable interface names.
# See https://major.io/2015/08/21/understanding-systemds-predictable-network-device-names/#comment-545626
# NICs for most Hetzner servers are not onboard, which is why we use
# `ID_NET_NAME_PATH`otherwise it would be `ID_NET_NAME_ONBOARD`.
INTERFACE_DEVICE_PATH=$(udevadm info -e | grep -Po "(?<=^P: )(.*${RESCUE_INTERFACE})")
UDEVADM_PROPERTIES_FOR_INTERFACE=$(udevadm info --query=property "--path=$INTERFACE_DEVICE_PATH")
NIXOS_INTERFACE=$(echo "$UDEVADM_PROPERTIES_FOR_INTERFACE" | grep -o -E 'ID_NET_NAME_PATH=\w+' | cut -d= -f2)
echo "WRONG WRONG WRONG (probably ens3) Determined NIXOS_INTERFACE as '$NIXOS_INTERFACE'"

# Determine network driver
NETWORK_MODULE=$(lshw -C network | grep -Poh 'driver=[[:alnum:]_]+' | sed -e 's/driver=//')
echo "Determined network driver to be ${NETWORK_MODULE}"
# driver=r8169

IP_V4=$(ip route get 8.8.8.8 | grep -Po '(?<=src )(\S+)')
echo "Determined IP_V4 as $IP_V4"

# Determine Internet IPv6 by checking route, and using ::1
# (because Hetzner rescue mode uses ::2 by default).
# The `ip -6 route get` output on Hetzner looks like:
#   # ip -6 route get 2001:4860:4860:0:0:0:0:8888
#   2001:4860:4860::8888 via fe80::1 dev eth0 src 2a01:4f8:151:62aa::2 metric 1024  pref medium
IP_V6="$(ip route get 2001:4860:4860:0:0:0:0:8888 | head -1 | cut -d' ' -f7 | cut -d: -f1-4)::1"
echo "Determined IP_V6 as $IP_V6"

# From https://stackoverflow.com/questions/1204629/how-do-i-get-the-default-gateway-in-linux-given-the-destination/15973156#15973156
read _ _ DEFAULT_GATEWAY _ < <(ip route list match 0/0); echo "$DEFAULT_GATEWAY"
echo "Determined DEFAULT_GATEWAY as $DEFAULT_GATEWAY"

HOSTNAME="nur-qws-vpn3"
echo "Hostname will be ${HOSTNAME}"

# See <https://nixos.wiki/wiki/NixOS_on_ZFS> for why we need the
# hostId and how to generate it
# head -c 8 /etc/machine-id
HOST_ID=$(head -c 8 /etc/machine-id)
echo "Host ID will be ${HOST_ID}"

# Generate `configuration.nix`. Note that we splice in shell variables.
cat > /mnt/etc/nixos/configuration.nix <<EOF
{ config, pkgs, ... }:
{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];
  # We want to still be able to boot without one of these
  fileSystems."/boot-1".options = [ "nofail" ];

  # Use GRUB2 as the boot loader.
  # We don't use systemd-boot because Hetzner uses BIOS legacy boot.
  boot.loader.systemd-boot.enable = false;
  boot.loader.grub = {
    enable = true;
    version = 2;
  };

  # This will mirror all UEFI files, kernels, grub menus and
  # things needed to boot to the other drive.
  boot.loader.grub.mirroredBoots = [
    { path = "/boot-1"; devices = [ "/dev/disk/by-id/scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-0-0-0" ]; }
  ];

  networking.hostName = "$HOSTNAME";

  # ZFS options from <https://nixos.wiki/wiki/NixOS_on_ZFS>
  networking.hostId = "$HOST_ID";
  boot.loader.grub.copyKernels = true;
  boot.supportedFilesystems = [ "zfs" ];

  # Network (Hetzner uses static IP assignments, and we don't use DHCP here)
  networking.useDHCP = false;
  networking.interfaces."$NIXOS_INTERFACE".ipv4.addresses = [
    {
      address = "$IP_V4";
      # Double check the below!
      prefixLength = 32;
    }
  ];
  networking.interfaces."$NIXOS_INTERFACE".ipv6.addresses = [
    {
      address = "$IP_V6";
      prefixLength = 64;
    }
  ];
  networking.defaultGateway = "$DEFAULT_GATEWAY";
  networking.defaultGateway6 = { address = "fe80::1"; interface = "$NIXOS_INTERFACE"; };
  networking.nameservers = [
    "185.12.64.1"
    "185.12.64.2"
  ];

  # Remote unlocking, see <https://nixos.wiki/wiki/NixOS_on_ZFS>,
  # section "Unlock encrypted zfs via ssh on boot"
  boot.initrd.availableKernelModules = [ "$NETWORK_MODULE" ];
  boot.kernelParams = [
    # See <https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt> for docs on this
    # ip=<client-ip>:<server-ip>:<gw-ip>:<netmask>:<hostname>:<device>:<autoconf>:<dns0-ip>:<dns1-ip>:<ntp0-ip>
    # The server ip refers to the NFS server -- we don't need it.
    "ip=$IP_V4::$DEFAULT_GATEWAY:255.255.255.255:$HOSTNAME-initrd:$NIXOS_INTERFACE:off:8.8.8.8"
  ];
  boot.initrd.network = {
    enable = true;
    ssh = {
       enable = true;
       port = 2222;
       # hostKeys paths must be unquoted strings, otherwise you'll run into issues
       # with boot.initrd.secrets the keys are copied to initrd from the path specified;
       # multiple keys can be set you can generate any number of host keys using
       # 'ssh-keygen -t ed25519 -N "" -f /boot-1/initrd-ssh-key'
       hostKeys = [
         /boot-1/initrd-ssh-key
       ];
       # public ssh key used for login
       authorizedKeys = [
         "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzJ+BIKXRDhZuX3/kfsVUQ4UoIVzm5lG/TicVWOpDEP2qOBQL1LA5mKRn0CnA7F3Vd+AWJo57NqEsuEw/WY0kzRSQXA3xqmLe2EEi9ZoAEUymHJvfaWXYl/rEcl1FslfczXHW3sKwhsdjHEC4Ikov7UGqsQfmkqNWbiLPLbSSmVso/kPHKG39MI8/QuKCK2NdVtvWm84l4PB7KMrEXNndMMkitgpGVCN9d3XBXSd+pqhdRPydN3y+/Zp9XoZh7aCDPOU9J/P9/4F3noTx3owuD0jk918K6kJihSoQWgXDIkjsN64Bp4tIme3ZcZNfcKHkKQWjoxQAlN9SL8wpasxXJrsxdYz15BR2zESA5/9zLuqSGqN9P71MG3IPepbPpZJucq2nSppkc0qiUJqS4pOWnpENHp+21/dryE2DRvfFe1may8VPzFZ2rzrKBgl6HDioTL01G+SjUnjpT2Mdp2lBs4FmNhOfHLJdpz0ghxIQBVOHZ2CXmW9OKYcyT9PkfTQ06NJD6mjRuwNatV801c5ZC099n04g0pd5rd15t7C1D9CSDUUP4MCjKBbT14pDcNnQdEcdtsZl2qqApbJPrkdGEZ1490bib8SsNfsYN7f5BjOQvtXSkDEQsZ+L9ZYBwSrU/zI6oeZ4jGIiSBDzgkeY/azg2Pl04HMn1VQNWqfMcYQ== ops@scvalex.net"
       ];
    };
    # this will automatically load the zfs password prompt on login
    # and kill the other prompt so boot can continue
    postCommands = ''
      cat <<EOG > /root/.profile
      if pgrep -x "zfs" > /dev/null
      then
        zpool import -f -a || true
        zfs load-key -a
        killall zfs
      else
        echo "zfs not running -- maybe the pool is taking some time to load for some unforseen reason."
      fi
      EOG
    '';
  };

  # Initial empty root password for easy login:
  users.users.root.initialHashedPassword = "";
  services.openssh.settings.PermitRootLogin = "prohibit-password";

  # SSH
  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCzJ+BIKXRDhZuX3/kfsVUQ4UoIVzm5lG/TicVWOpDEP2qOBQL1LA5mKRn0CnA7F3Vd+AWJo57NqEsuEw/WY0kzRSQXA3xqmLe2EEi9ZoAEUymHJvfaWXYl/rEcl1FslfczXHW3sKwhsdjHEC4Ikov7UGqsQfmkqNWbiLPLbSSmVso/kPHKG39MI8/QuKCK2NdVtvWm84l4PB7KMrEXNndMMkitgpGVCN9d3XBXSd+pqhdRPydN3y+/Zp9XoZh7aCDPOU9J/P9/4F3noTx3owuD0jk918K6kJihSoQWgXDIkjsN64Bp4tIme3ZcZNfcKHkKQWjoxQAlN9SL8wpasxXJrsxdYz15BR2zESA5/9zLuqSGqN9P71MG3IPepbPpZJucq2nSppkc0qiUJqS4pOWnpENHp+21/dryE2DRvfFe1may8VPzFZ2rzrKBgl6HDioTL01G+SjUnjpT2Mdp2lBs4FmNhOfHLJdpz0ghxIQBVOHZ2CXmW9OKYcyT9PkfTQ06NJD6mjRuwNatV801c5ZC099n04g0pd5rd15t7C1D9CSDUUP4MCjKBbT14pDcNnQdEcdtsZl2qqApbJPrkdGEZ1490bib8SsNfsYN7f5BjOQvtXSkDEQsZ+L9ZYBwSrU/zI6oeZ4jGIiSBDzgkeY/azg2Pl04HMn1VQNWqfMcYQ== ops@scvalex.net"
  ];
  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    ports = [ 2022 ];
  };

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.05"; # Did you read the comment?
}
EOF

# Install NixOS
nixos-install --no-root-passwd --root /mnt --max-jobs 20

umount /mnt/boot-1
umount /mnt
umount /nix
zfs destroy zroot/tmp-install

systemctl reboot
