{
  config,
  lib,
  pkgs,
  ...
}:
with lib;
let
  cfg = config.ab.networking;
  machineCfg = config.ab.machine;
  wgCfg = config.ab.services.wireguard;
  kubeCfg = config.ab.services.kubernetes;
in
{
  imports = [ ./ab-machine.nix ];

  options = {
    ab.networking = {
      enable = mkEnableOption "networking";

      vpn = mkEnableOption "Enable VPN gateway";

      lan = mkOption {
        type = with types; nullOr str;
        description = "Name of the physical lan this machine is on";
        default = null;
      };

      publicInterface = mkOption {
        type = types.str;
        description = "Public interface";
      };

      ipAddress = mkOption {
        type = with types; nullOr str;
        description = "Public IPv4 address";
        default = null;
      };

      lanIpAddress = mkOption {
        type = with types; nullOr str;
        description = "LAN IPv4 address";
        default = null;
      };

      netmask4 = mkOption {
        type = types.int;
        default = 32;
        description = "IPv4 netmask";
      };

      netmask4DotNotation = mkOption {
        type = types.str;
        default = "255.255.255.255";
        description = "IPv4 netmask";
      };

      ip6Address = mkOption {
        type = with types; nullOr str;
        description = "IPv6 address";
        default = null;
      };

      netmask6 = mkOption {
        type = types.int;
        default = 64;
        description = "IPv6 netmask";
      };

      defaultGateway = mkOption {
        type = types.str;
        description = "Default gateway (required for root servers)";
      };
    };
  };

  config = mkMerge [
    (mkIf cfg.enable {
      networking.useDHCP = false;

      networking.nat =
        if cfg.vpn then
          {
            enable = true;
            enableIPv6 = true;
            externalInterface = cfg.publicInterface;
            internalInterfaces = [ wgCfg.wgInterface ];
          }
        else
          { };

      networking.firewall.allowedUDPPorts = [
        32222 # syncthing
      ];
      networking.firewall.allowedTCPPorts = [
        32222 # syncthing
      ];
    })
    (mkIf kubeCfg.master.enable {
      # Fix NixOS's default rules of accepting and forwarding everything.
      #
      # The accepting thing is actually fine because there's a rule at
      # the end of the INPUT chain that logs and refuses everything.
      #
      # We have to either do that for the FORWARD chain ourselves, or
      # change the policy to drop.  Here we do the latter because it
      # seems easier.
      #
      # Flannel needs the kubernetes rules.  This instructs each machine
      # to forward packets originating from the pods.  Flannel claims to
      # do this anyway, but I don't trust it.  I think it's actually
      # kube-proxy who's doing it.
      networking.nftables.ruleset = ''
        # Kube-proxy interacts with the FORWARD chain in the filter
        # table.  We can't change this, so we just change the policy
        # to drop.
        table ip filter {
              chain FORWARD {
                    type filter hook forward priority filter; policy drop;
                    ct state established,related accept
                    ip daddr ${kubeCfg.clusterCidrIp4} counter accept
                    ip saddr ${kubeCfg.clusterCidrIp4} counter accept
              }
        }
        table ip6 filter {
              chain FORWARD {
                    type filter hook forward priority filter; policy drop;
                    ct state established,related accept
              }
        }
      '';
    })
  ];
}
