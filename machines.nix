# Wireguard key gen:
#   wg genkey | tee privatekey | wg pubkey > publickey
{ pkgs }:
let
  readKey =
    hostnameDotSomething:
    with pkgs.lib;
    replaceStrings [ "\n" ] [ "" ] (readFile (./. + "/secrets/wireguard/${hostnameDotSomething}"));
  allDomains = [
    "scvalex.net"
    "umami.scvalex.net"
    "abstractbinary.org"
    "foundry-ftp.abstractbinary.org"
    "foundry.abstractbinary.org"
    "files.abstractbinary.org"
  ];
in
with pkgs.lib;
{
  nur-aws-vpn5 = {
    ipAddress = "167.235.30.149";
    ip6Address = "2a01:4f8:1c1b:a5cf::1";
    publicInterface = "enp1s0";

    wireguard = {
      ipAddress = "10.10.0.19";
      ip6Address = "fd86:ea04:1111::19";
      publicKey = readKey "nur-aws-vpn5.pub";
      privateKey = readKey "nur-aws-vpn5.priv";
    };
  };

  nur-apr-matrix1 = {
    ipAddress = "78.47.26.7";
    ip6Address = "2a01:4f8:1c1b:ffce::1";
    publicInterface = "enp1s0";

    wireguard = {
      ipAddress = "10.10.0.22";
      ip6Address = "fd86:ea04:1111::22";
      publicKey = readKey "nur-apr-matrix1.pub";
      privateKey = readKey "nur-apr-matrix1.priv";
    };
  };

  laptop = {
    lan = "edb";

    wireguard = {
      ipAddress = "10.10.0.2";
      ip6Address = "fd86:ea04:1111::2";
      publicKey = readKey "laptop.pub";
      privateKey = readKey "laptop.priv";
    };
  };

  sana = {
    lan = "edb";

    wireguard = {
      ipAddress = "10.10.0.21";
      ip6Address = "fd86:ea04:1111::21";
      publicKey = readKey "sana.pub";
      privateKey = readKey "sana.priv";
    };
  };

  cibo = {
    lan = "edb";
    lanIpAddress = "192.168.10.183";
    lanInterface = "enp8s0u5c2";

    wireguard = {
      ipAddress = "10.10.0.13";
      ip6Address = "fd86:ea04:1111::13";
      publicKey = readKey "cibo.pub";
      privateKey = readKey "cibo.priv";
    };
  };

  edb-qws-pi2 = {
    lanIpAddress = "192.168.10.217";
    lan = "edb";
    lanInterface = "eth0";

    wireguard = {
      ipAddress = "10.10.0.15";
      ip6Address = "fd86:ea04:1111::15";
      publicKey = readKey "edb-qws-pi2.pub";
      privateKey = readKey "edb-qws-pi2.priv";
    };
  };

  fsn-qpr-kube1 = {
    ipAddress = "188.245.204.195";
    ip6Address = "2a01:4f8:c012:e148::1";
    publicInterface = "enp1s0";
    domains = allDomains;

    wireguard = {
      ipAddress = "10.10.0.18";
      ip6Address = "fd86:ea04:1111::18";
      publicKey = readKey "fsn-qpr-kube1.pub";
      privateKey = readKey "fsn-qpr-kube1.priv";
    };
  };

  fsn-qpr-kube2 = {
    ipAddress = "49.13.60.20";
    ip6Address = "2a01:4f8:c012:8adb::1";
    publicInterface = "enp1s0";
    domains = allDomains;

    wireguard = {
      ipAddress = "10.10.0.17";
      ip6Address = "fd86:ea04:1111::17";
      publicKey = readKey "fsn-qpr-kube2.pub";
      privateKey = readKey "fsn-qpr-kube2.priv";
    };
  };

  fsn-qpr-kube3 = {
    ipAddress = "188.245.56.19";
    ip6Address = "2a01:4f8:c17:2c8d::1";
    publicInterface = "enp1s0";
    domains = allDomains;

    wireguard = {
      ipAddress = "10.10.0.16";
      ip6Address = "fd86:ea04:1111::16";
      publicKey = readKey "fsn-qpr-kube3.pub";
      privateKey = readKey "fsn-qpr-kube3.priv";
    };
  };

  phone2 = {
    wireguard = {
      ipAddress = "10.10.0.9";
      ip6Address = "fd86:ea04:1111::9";
      publicKey = "KcjyEvIuNdkzIPYv+dR2rgyPFi7c/IjTXvplMYWCDj4=";
    };
  };

  fp4 = {
    wireguard = {
      ipAddress = "10.10.0.14";
      ip6Address = "fd86:ea04:1111::14";
      publicKey = "TrXBUToN/up+D+OrnfZG0JkRMrotnZe6e8qpfGoXXno=";
    };
  };

  nur-apr-app1 = {
    ipAddress = "157.90.166.239";
    ip6Address = "2a01:4f8:c0c:eea4::1";
  };

  nur-apr-bgt1 = {
    ipAddress = "138.199.210.137";
    ip6Address = "2a01:4f8:1c1c:1452::1";
  };
}
