# Add a new member to the cluster:
# etcdctl member add fsn-qws-app1 --peer-urls=https://10.10.0.10:2380,https://fsn-qws-app1:2380

{
  config,
  lib,
  pkgs,
  nodes,
  ...
}:

with lib;
let
  cfg = config.ab.services.etcd;
  machineCfg = config.ab.machine;
  wgCfg = config.ab.services.wireguard;
  clientUrls = [
    "https://localhost:2379"
    "https://${cfg.ipAddress}:2379"
    "https://[${cfg.ip6Address}]:2379"
  ];
  mkSslKey = name: {
    text = readFile (./secrets/ssl + "/${name}");
    user = "etcd";
    group = "nogroup";
  };
  secretsPath = "etcd/secret";
  pathToSecret = name: "/etc/${secretsPath}/${name}";
  etcdSecrets = {
    etcd_key = mkSslKey "etcd-${config.networking.hostName}-key.pem";
    etcd_pem = mkSslKey "etcd-${config.networking.hostName}.pem";
  };
in
{
  imports = [ ];

  options = {
    ab.services.etcd = {
      enable = mkEnableOption "Etcd key-value store";

      ipAddress = mkOption {
        type = types.str;
        default = wgCfg.ipAddress;
        description = "Etcd IPv4 address";
      };

      ip6Address = mkOption {
        type = types.str;
        default = wgCfg.ip6Address;
        description = "Etcd IPv6 address";
      };
    };
  };

  config = mkIf cfg.enable {
    assertions = [
      {
        assertion = machineCfg.encryptedRoot;
        message = "Etcd machines must have encrypted roots";
      }
    ];

    systemd.services.etcd.after = [ "sys-devices-virtual-net-wg0.device" ];
    systemd.services.etcd.requires = [ "sys-devices-virtual-net-wg0.device" ];
    systemd.services.etcd.wantedBy = [ "multi-user.target" ];
    systemd.services.etcd.serviceConfig.Restart = "always";
    systemd.services.etcd.unitConfig.StartLimitIntervalSec = 0;

    environment.etc = mapAttrs' (
      name: value: nameValuePair "${secretsPath}/${name}" (value // { mode = "0400"; })
    ) etcdSecrets;

    services.etcd = {
      enable = true;
      package = pkgs.etcd_3_5;
      name = config.networking.hostName;

      clientCertAuth = true;
      peerClientCertAuth = true;
      peerCertFile = pathToSecret "etcd_pem";
      peerKeyFile = pathToSecret "etcd_key";
      peerTrustedCaFile = "/etc/abstractbinary-ca.pem";
      trustedCaFile = "/etc/abstractbinary-ca.pem";
      certFile = pathToSecret "etcd_pem";
      keyFile = pathToSecret "etcd_key";

      listenPeerUrls = [ "https://${cfg.ipAddress}:2380" ];

      listenClientUrls = clientUrls;
      # We need to explicitly set `advertiseClientUrls` here.  Relying
      # on the default in etcd.nix doesn't work because other modules
      # (e.g. apiserver.nix) might try to helpfully set a different
      # default value.
      advertiseClientUrls = clientUrls;

      initialAdvertisePeerUrls = flatten (
        mapAttrsToList (
          hostname: machine:
          if machine.config.ab.services.etcd.enable then [ "https://${hostname}.wg:2380" ] else [ ]
        ) nodes
      );

      initialCluster = flatten (
        mapAttrsToList (
          hostname: machine:
          let
            machine_cfg = machine.config.ab.services.etcd;
          in
          if machine_cfg.enable then
            [
              "${hostname}=https://${hostname}.wg:2380"
              "${hostname}=https://${machine_cfg.ipAddress}:2380"
            ]
          else
            [ ]
        ) nodes
      );

      initialClusterState = "existing";
    };
  };
}
