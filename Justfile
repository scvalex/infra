default:
	just --choose

# build deployment for all hosts
build:
	nice ./ops build --keep-result --on "@real"

# build deployment for target host
build-on $target:
	nice ./ops build --keep-result --on "$target"

# build an installer iso image based on iso.nix
build-iso:
	nix-build '<nixpkgs/nixos>' -A config.system.build.isoImage -I nixos-config=iso.nix

# push closures, but don't activate
push:
	./ops apply --keep-result push --on '@real'

# deploy to the local machine at next book
apply-local:
	./ops apply-local --sudo boot

# deploy to the local machine now
apply-local-now:
	./ops apply-local --sudo

# deploy and activate without restarting
deploy-now $target:
	echo "Deploying live to $target"
	./ops apply --on "$target" --keep-result

# update DNS
dns:
	nix run .#octodns

# generate new certificates (not replacing existing)
gen-certs:
	nix run .#genCerts secrets/

# update a flake input
update-input $input:
	nix flake update "$input"

# gracefully upgrade the given host
upgrade $target:
	echo "Upgrading $target"
	./ops build --on "$target" --keep-result
	echo "[ \"$target\" ]" > disabled-frontends.nix
	nix run .#octodns
	if [[ $target == *-*-vpn* ]]; then sudo networkctl down wg0; fi
	./ops apply --on "$target" --keep-result boot
	./ops exec --on "$target" -- systemctl reboot
	grep "unlock-$target" ~/.ssh/config && ( ssh -v -o ConnectTimeout=1 -o ConnectionAttempts=300 "unlock-$target" || true ) || true
	just finish-upgrade $target

# complete an upgrade after rebooting
finish-upgrade $target:
	ssh -o ConnectTimeout=1 -o ConnectionAttempts=180 "$target" echo "Host is up"
	./ops upload-keys --on "$target"
	if [[ $target == *-*-vpn* ]]; then sudo networkctl up wg0; fi
	echo "[ ]" > disabled-frontends.nix
	nix run .#octodns
