# After setting up the database, run
#
# # sudo -u postgres psql
# postgres=# GRANT pg_read_all_data TO grafana;
#
# Upgrading (even minor upgrades):
# - comment out the `ensureDatabase` and `ensureUsers` part of the config
# - run:
#
# # psql -U env_sensor -h localhost -X
# env_sensor=> alter extension timescaledb update;
# # psql -U postgres -h localhost -d postgres -X
# postgres=# alter extension timescaledb update;
#
# - re-enable the `ensure*` configs

{
  config,
  lib,
  pkgs,
  env-sensor,
  ...
}:

with lib;
let
  cfg = config.ab.services.iot-hub;
  netCfg = config.ab.networking;
  env-sensor-hub = env-sensor.packages."${config.nixpkgs.system}".hub;
in
{
  imports = [ ];

  options = {
    ab.services.iot-hub = {
      enable = mkEnableOption "IOT Hub";
      listenPort = mkOption {
        type = types.port;
        default = 5555;
        description = "Port to listen on for updates";
      };
      grafanaPort = mkOption {
        type = types.port;
        default = 5556;
        description = "Port for grafana to listen on";
      };
    };
  };

  config = mkIf cfg.enable {
    assertions = [ ];

    networking.firewall.allowedTCPPorts = [ cfg.grafanaPort ];
    networking.firewall.allowedUDPPorts = [ cfg.listenPort ];

    systemd.services.env-sensor-hub = {
      wantedBy = [ "multi-user.target" ];
      restartIfChanged = true;
      serviceConfig = {
        ExecStart = "${env-sensor-hub}/bin/env-sensor-hub server";
        Restart = "always";
      };
      unitConfig = {
        StartLimitIntervalSec = 0;
      };
      environment = {
        PORT = "${toString cfg.listenPort}";
        DATABASE_URL = "postgres://env_sensor@localhost:5432/env_sensor";
      };
      confinement.enable = true;
    };

    services.postgresql = {
      enable = true;
      package = pkgs.postgresql_14;
      extraPlugins = with pkgs.postgresql14Packages; [ timescaledb ];
      settings.shared_preload_libraries = "timescaledb";
      enableTCPIP = false; # TCP over lo should still work
      authentication = ''
        host env_sensor env_sensor localhost trust
        host env_sensor env_sensor localhost password
        host postgres postgres localhost trust
      '';
      ensureDatabases = [ "env_sensor" ];
      ensureUsers = [
        {
          name = "env_sensor";
          ensureDBOwnership = true;
        }
        { name = "grafana"; }
      ];
    };

    services.grafana = {
      # enable = true;
      enable = false;
      settings = {
        server = {
          http_addr = "${netCfg.lanIpAddress}";
          http_port = cfg.grafanaPort;
        };
      };
    };
  };
}
