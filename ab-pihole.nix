{
  config,
  lib,
  pkgs,
  ...
}:

with lib;
let
  cfg = config.ab.services.pihole;
in
{
  imports = [ ];

  options = {
    ab.services.pihole.enable = mkEnableOption "pi-hole DNS server";
  };

  config = mkIf cfg.enable {
    virtualisation.oci-containers.backend = "podman";
    virtualisation.oci-containers.containers = {
      # https://github.com/pi-hole/docker-pi-hole/#running-pi-hole-docker
      pihole = {
        image = "docker.io/pihole/pihole:2023.03.0";
        ports = [
          "53:53/tcp"
          "53:53/udp"
          "80:80/tcp"
        ];
        volumes = [
          "/var/lib/pihole/etc:/etc/pihole"
          "/var/lib/pihole/dnsmasq-etc:/etc/dnsmasq.d"
        ];
        environment = {
          "TZ" = "Europe/London";
          "WEBPASSWORD" = removeSuffix "\n" (readFile (./secrets/passwords + "/pihole"));
          "VIRTUAL_HOST" = config.networking.hostName + ".lan";
        };
        extraOptions = [
          "--dns=127.0.0.1"
          "--dns=8.8.8.8"
        ];
        autoStart = true;
      };
    };
    system.activationScripts = {
      create-pihole-var = ''
        mkdir -p /var/lib/pihole/etc /var/lib/pihole/dnsmasq-etc
      '';
    };
  };
}
